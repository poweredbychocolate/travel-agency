## Travel agency java ee project. 

# Description
Travel Agency - java ee project.
# What's used   
1. PrimeFaces
2. JBoss EJB
3. Hibernate jpa, Hibernate jpamodelgen
4. java mail and itextpdf 7
5. Wildfly 12 and MariaDB 10

# Configuration 
1. java:/TravelAgency mariaDB JNDI
2. java:/TravelAgencyGMail mail session using gmail account JNDI

# TODO 
1. Add more content to pdf 
2. Add external supplier for hotels and planes. 
