package bean;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import entries.hotels.HotelReservation;
import entries.planes.PlaneTicket;
import entries.travel.TravelOffer;
import org.primefaces.event.SelectEvent;
import enterpriseBean.client.local.TravelOfferBeanLocal;

/**
 * @author Dawid
 * @since 26.04.2018
 * @version 1.0
 */
@Named
@SessionScoped
public class TravelBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private TravelOfferBeanLocal travelOfferBean;
	@Inject
	private ClientAccountBean clientAccountBean;

	private TravelOffer selectedTravelOffer = null;

	private PlaneTicket selectedPlane = null;
	private HotelReservation selectedHotel = null;
	private List<PlaneTicket> planesList;
	private List<HotelReservation> hotelsList;

	private int minHotelPrice = 0;
	private int minPlanePrice = 0;
	private String sortOrder;
	private String selectedCity;

	private HashMap<String, String> sortMap;
	private HashMap<String, String> cityList;

	/**
	 * 
	 */
	public TravelBean() {

	}

	@PostConstruct
	private void init() {
		sortMap = new LinkedHashMap<>();
		sortMap.put("Tytuł:Rosnąco", TravelOffer.SORT_TITLE_ASC);
		sortMap.put("Tytuł:Malejąco", TravelOffer.SORT_TITLE_DESC);
		sortMap.put("Data:Rosnąco", TravelOffer.SORT_DATE_ASC);
		sortMap.put("Data:Malejąco", TravelOffer.SORT_DATE_DESC);
		sortMap.put("Cena:Rosnąco", TravelOffer.SORT_PRICE_ASC);
		sortMap.put("Cena:Malejąco", TravelOffer.SORT_PRICE_DESC);

	}

	/**
	 * @return the selectedPlane
	 */
	public PlaneTicket getSelectedPlane() {
		return selectedPlane;
	}

	/**
	 * @param selectedPlane
	 *            the selectedPlane to set
	 */
	public void setSelectedPlane(PlaneTicket selectedPlane) {
		this.selectedPlane = selectedPlane;
	}

	/**
	 * @return the selectedHotel
	 */
	public HotelReservation getSelectedHotel() {
		return selectedHotel;
	}

	/**
	 * @param selectedHotel
	 *            the selectedHotel to set
	 */
	public void setSelectedHotel(HotelReservation selectedHotel) {
		this.selectedHotel = selectedHotel;
	}

	public TravelOffer getSelectedTravelOffer() {
		String travelParam = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("travel");
		if (travelParam != null) {
			initUsingParameters(travelParam);
		}
		return selectedTravelOffer;
	}

	private void initUsingParameters(String travelParam) {
		if (selectedTravelOffer == null
				|| (selectedTravelOffer != null && !travelParam.equals(selectedTravelOffer.getId().toString()))) {

			try {
				Integer id = Integer.parseInt(travelParam);
				this.selectedTravelOffer = this.travelOfferBean.findFullyTravelOffer(id);
				if (this.selectedTravelOffer.getType().equals(TravelOffer.TYPE_ACTIVE)) {
					checkHotelsAvailability();
					selectedHotel = hotelsList.stream().min(Comparator.comparing(HotelReservation::getPrice)).get();
					checkPlanesAvailability();
					selectedPlane = planesList.stream().min(Comparator.comparing(PlaneTicket::getTicketPrice)).get();

					minHotelPrice = selectedHotel.getPrice();
					minPlanePrice = selectedPlane.getTicketPrice();
				} else {
					this.selectedTravelOffer = null;
				}
			} catch (Exception e) {
			}
		}
	}

	public void setSelectedTravelOffer(TravelOffer selectedTravelOffer) {
		this.selectedTravelOffer = selectedTravelOffer;
	}

	/**
	 * @return the travelOfferBean
	 */
	public TravelOfferBeanLocal getTravelOfferBean() {
		return travelOfferBean;
	}

	/**
	 * @return the userAccountBean
	 */
	public ClientAccountBean getClientAccountBean() {
		return clientAccountBean;
	}

	/**
	 * @param clientAccountBean
	 *            the clientAccountBean to set
	 */
	public void setClientAccountBean(ClientAccountBean clientAccountBean) {
		this.clientAccountBean = clientAccountBean;
	}

	/**
	 * @return the minHotelPrice
	 */
	public int getMinHotelPrice() {
		return minHotelPrice;
	}

	/**
	 * @return the minPlanePrice
	 */
	public int getMinPlanePrice() {
		return minPlanePrice;
	}

	public List<TravelOffer> getOffers() {
		return travelOfferBean.getAllActiveOffer(selectedCity, sortOrder);
	}

	public List<TravelOffer> getHottestOffer() {
		return travelOfferBean.getNewestOffer(5);
	}

	public void onHotelSelect(SelectEvent event) {
		checkPlanesAvailability();
		System.err.println(((HotelReservation) event.getObject()).toString());
	}

	public void onPlaneSelect(SelectEvent event) {
		System.err.println(((PlaneTicket) event.getObject()).toString());
	}

	public String openOffer() {
		selectedTravelOffer = travelOfferBean.findFullyTravelOffer(selectedTravelOffer.getId());
		// hotelsList = selectedTravelOffer.getHotelsList();
		// planesList = selectedTravelOffer.getPlanesList();

		checkHotelsAvailability();
		selectedHotel = hotelsList.stream().min(Comparator.comparing(HotelReservation::getPrice)).get();
		checkPlanesAvailability();
		selectedPlane = planesList.stream().min(Comparator.comparing(PlaneTicket::getTicketPrice)).get();

		minHotelPrice = selectedHotel.getPrice();
		minPlanePrice = selectedPlane.getTicketPrice();
		////

		return "travelsDetails?faces-redirect=true";
	}

	private void checkHotelsAvailability() {

		hotelsList = selectedTravelOffer.getHotelsList().stream()
				.filter(h -> h.getRoomCount() - h.getSoldRoomCount() > 0).sorted(Comparator
						.comparing(HotelReservation::getPrice).thenComparing(HotelReservation::getPersonInRoom))
				.collect(Collectors.toList());
	}

	private void checkPlanesAvailability() {

		planesList = selectedTravelOffer.getPlanesList().stream()
				.filter(p -> p.getTicketCount() - p.getSoldTicket() >= selectedHotel.getPersonInRoom())
				.sorted(Comparator.comparing(PlaneTicket::getTicketPrice).thenComparing(PlaneTicket::getTicketCount))
				.collect(Collectors.toList());
	}

	public boolean checkAvailability() {
		return hotelsList.size() > 0 && planesList.size() > 0;
	}

	/**
	 * @return the sortOrder
	 */
	public String getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder
	 *            the sortOrder to set
	 */
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public HashMap<String, String> sortOrderList() {
		return sortMap;
	}

	public void onSortOrderChanged() {
		System.err.println("[SortOrderChanged]" + sortOrder);

	}

	/**
	 * @return the selectedCity
	 */
	public String getSelectedCity() {
		return selectedCity;
	}

	/**
	 * @param selectedCity
	 *            the selectedCity to set
	 */
	public void setSelectedCity(String selectedCity) {
		this.selectedCity = selectedCity;
	}

	/**
	 * @return the cityList
	 */
	public HashMap<String, String> cityList() {
		cityList = new LinkedHashMap<>();
		cityList.put("Wszystkie", null);
		for (String city : travelOfferBean.travelCities()) {
			cityList.put(city, city);
		}
		return cityList;
	}

	/**
	 * @return the planesList
	 */
	public List<PlaneTicket> getPlanesList() {
		return planesList;
	}

	/**
	 * @param planesList
	 *            the planesList to set
	 */
	public void setPlanesList(List<PlaneTicket> planesList) {
		this.planesList = planesList;
	}

	/**
	 * @return the hotelsList
	 */
	public List<HotelReservation> getHotelsList() {
		return hotelsList;
	}

	/**
	 * @param hotelsList
	 *            the hotelsList to set
	 */
	public void setHotelsList(List<HotelReservation> hotelsList) {
		this.hotelsList = hotelsList;
	}

}
