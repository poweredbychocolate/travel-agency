package bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.PrimeFaces;
import classic.TokenGenerator;
import classic.mails.RegistrationMail;
import enterpriseBean.client.local.ClientUserBeanLocal;
import enterpriseBean.dataProcessing.local.MailBeanLocal;
import entries.client.ActivationToken;
import entries.client.ClientAccount;
import entries.client.Person;

/**
 * @author Dawid
 * @version 1.0
 * @since 15.05.2018
 *
 */
@SessionScoped
@Named(value = "userBean")
public class ClientAccountBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ClientUserBeanLocal clientBean;
	@EJB
	private MailBeanLocal mailBean;

	private HttpServletRequest RequestHTTP;
	private TokenGenerator tokenGenerator;

	private ClientAccount client;
	private String mail;
	private String password;
	private String confirmPassword;

	private Date dateOfBirth;
	private String firstName;
	private String lastName;
	private String street;
	private String houseNumber;
	private String apartmentNumber;
	private String postalCode;
	private String city;
	private String country;

	private Boolean newsletterNewOffer;
	private Boolean newsletterSelectedCountry;
	private String newsletterCountry;
	private Integer newsletterCounter;

	public ClientAccountBean() {
		this.tokenGenerator = new TokenGenerator();
		resetNewsletterCounter();
	}

	/**
	 * @return the client
	 */
	public ClientAccount getClient() {
		return client;
	}

	/**
	 * @param client
	 *            the client to set
	 */
	public void setClient(ClientAccount client) {
		this.client = client;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail
	 *            the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the confirmPassword
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * @param confirmPassword
	 *            the confirmPassword to set
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	/**
	 * @return the clientBean
	 */
	public ClientUserBeanLocal getClientBean() {
		return clientBean;
	}

	/**
	 * @param clientBean
	 *            the clientBean to set
	 */
	public void setClientBean(ClientUserBeanLocal clientBean) {
		this.clientBean = clientBean;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the houseNumber
	 */
	public String getHouseNumber() {
		return houseNumber;
	}

	/**
	 * @param houseNumber
	 *            the houseNumber to set
	 */
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	/**
	 * @return the apartmentNumber
	 */
	public String getApartmentNumber() {
		return apartmentNumber;
	}

	/**
	 * @param apartmentNumber
	 *            the apartmentNumber to set
	 */
	public void setApartmentNumber(String apartmentNumber) {
		this.apartmentNumber = apartmentNumber;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Redirect to user account register page
	 * 
	 * @return redirect page
	 */
	public String registerPage() {

		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		RequestHTTP = ((HttpServletRequest) ec.getRequest());
		return "clientRegistration?faces-redirect=true";
	}

	/**
	 * Try find user account using mail and password, on success close dialog and
	 * redirect to current page otherwise display user not found message
	 */
	public void userLogin() {
		client = clientBean.findUser(mail, tokenGenerator.hashString(password));
		if (client == null) {
			FacesContext.getCurrentInstance().addMessage("mail",
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Nieprawidłowe dane logowania", null));
		} else {
			// close login dialog
			password = null;
			confirmPassword = null;
			newsletterNewOffer = client.getNewsletter().getNewOffer();
			newsletterCountry = client.getNewsletter().getTravelCountry();
			newsletterSelectedCountry = newsletterCountry != null;
			PrimeFaces.current().executeScript("PF('userLogin').hide()");
			reloadPage();
		}
	}

	/**
	 * Logout current user, redirect to current page
	 */
	public void userLogout() {
		this.client = null;
		clean();
		reloadPage();
	}

	private void reloadPage() {
		try {
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create new user account using form fields
	 * 
	 * @return page registration 
	 */
	public String userRegistration() {
		if (password.equals(confirmPassword)) {
			ClientAccount clientAccount = new ClientAccount();
			clientAccount.setMail(mail);
			clientAccount.setPassword(tokenGenerator.hashString(password));
			Person person = new Person();
			person.setFirstName(firstName);
			person.setLastName(lastName);
			person.setDateOfBirth(dateOfBirth);

			person.setCountry(country);
			person.setPlace(city);
			person.setStreet(street);
			person.setHouseNumber(houseNumber);
			person.setApartmentNumber(apartmentNumber);
			person.setPostalCode(postalCode);
			clientAccount.setClientData(person);
			clientAccount = clientBean.saveClientAccount(clientAccount);
			if (clientAccount != null) {
				// System.err.println(clientAccount.toString());
				ActivationToken activationToken = new ActivationToken();
				activationToken.setClientAccount(clientAccount);
				activationToken.setToken(tokenGenerator.randomToken(100));
				activationToken.setGeneratedDate(new Date());
				clientBean.saveActivationToken(activationToken);
				//// get path that allow open recipe using servlet
				HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
						.getRequest();
				String url = req.getRequestURL().toString();
				String path = url.substring(0, url.length() - req.getRequestURI().length()) + req.getContextPath()
						+ "/Client";
				//// ] get path that allow open recipe using servlet
				////
				mailBean.sendMail(new RegistrationMail(activationToken, path));
				clean();
				try {
					ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
					ec.redirect(RequestHTTP.getRequestURI());
				} catch (Exception e) {
					return "mainPage?faces-redirect=true";
				}
			} else {
				FacesContext.getCurrentInstance().addMessage("mail",
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Adres e-mail używany przez inne konto ", null));
			}
		} else {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Hasła nie zgadzają się", null);
			FacesContext.getCurrentInstance().addMessage("pass", msg);
			FacesContext.getCurrentInstance().addMessage("conf", msg);
		}

		return null;
	}

	/**
	 * Change user password
	 */
	public void changePassword() {
		if (password.equals(confirmPassword)) {
			client.setPassword(tokenGenerator.hashString(password));
			clientBean.saveClientAccount(client);
			password = null;
			confirmPassword = null;
			PrimeFaces.current().executeScript("PF('userPass').hide()");
		} else {
			FacesContext.getCurrentInstance().addMessage("ch_pass",
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Hasła nie zgadzają się", null));
			password = null;
			confirmPassword = null;
		}
	}

	private void clean() {
		mail = null;
		password = null;
		confirmPassword = null;

		dateOfBirth = null;
		firstName = null;
		lastName = null;
		street = null;
		houseNumber = null;
		apartmentNumber = null;
		postalCode = null;
		city = null;
		country = null;

		newsletterCountry = null;
		newsletterNewOffer = false;
		newsletterSelectedCountry = false;
	}

	/**
	 * @return the newsletterNewOffer
	 */
	public Boolean getNewsletterNewOffer() {
		return newsletterNewOffer;
	}

	/**
	 * @param newsletterNewOffer
	 *            the newsletterNewOffer to set
	 */
	public void setNewsletterNewOffer(Boolean newsletterNewOffer) {
		this.newsletterNewOffer = newsletterNewOffer;
	}

	/**
	 * @return the newsletterCity
	 */
	public String getNewsletterCountry() {
		return newsletterCountry;
	}

	/**
	 * @param newsletterCountry
	 *            the newsletterCountry to set
	 */
	public void setNewsletterCountry(String newsletterCountry) {
		this.newsletterCountry = newsletterCountry;
	}

	/**
	 * @return the newsletterSelectedCountry
	 */
	public Boolean getNewsletterSelectedCountry() {
		return newsletterSelectedCountry;
	}

	/**
	 * @param newsletterSelectedCountry
	 *            the newsletterSelectedCountry to set
	 */
	public void setNewsletterSelectedCountry(Boolean newsletterSelectedCountry) {
		this.newsletterSelectedCountry = newsletterSelectedCountry;
	}

	@Schedule(second = "*/1", minute = "*", hour = "*", persistent = false)
	public void updateNewsletterCounter() {
		if (newsletterCounter == 1) {
			clientBean.saveClientAccount(client);
			newsletterNewOffer = client.getNewsletter().getNewOffer();
			newsletterCountry = client.getNewsletter().getTravelCountry();
		}

		if (newsletterCounter > 0) {
			newsletterCounter--;
		}
	}

	public void resetNewsletterCounter() {
		newsletterCounter = 5;
	}

	public void onNewsletterChange() {
		System.err.println("[New Offer Change]");

		client.getNewsletter().setNewOffer(newsletterNewOffer);
		client.getNewsletter().setTravelCountry(newsletterCountry);
		resetNewsletterCounter();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserAccountBean [clientBean=" + clientBean + ", client=" + client + ", mail=" + mail + ", password="
				+ password + ", confirmPassword=" + confirmPassword + ", dateOfBirth=" + dateOfBirth + ", firstName="
				+ firstName + ", lastName=" + lastName + ", street=" + street + ", houseNumber=" + houseNumber
				+ ", apartmentNumber=" + apartmentNumber + ", postalCode=" + postalCode + ", city=" + city
				+ ", country=" + country + "]";
	}

}
