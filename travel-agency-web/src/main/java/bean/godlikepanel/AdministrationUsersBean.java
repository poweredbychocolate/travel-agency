/**
 * 
 */
package bean.godlikepanel;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import classic.TokenGenerator;
import classic.mails.AdminstrationUserMail;
import enterpriseBean.administration.local.AdministrationUsersBeanLocal;
import enterpriseBean.dataProcessing.local.MailBeanLocal;
import entries.adminstration.AdministrationUser;

/**
 * @author Dawid
 * @since 29.04.2018
 * @version 1.0
 */
@Named
@SessionScoped
public class AdministrationUsersBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private AdministrationUsersBeanLocal userBean;
	@EJB
	private MailBeanLocal mailBean;
	private AdministrationUser selectedUser;

	private String newPassword = null;
	private String confirmPassword = null;
	private String mail = null;
	private boolean locked = false;
	private String adminstrationIdentifier = AdministrationUser.PREFIX;
	private boolean usersAccess = false;
	private boolean travelAccess = false;
	private boolean hotelAccess = false;
	private boolean transportAccess = false;

	private TokenGenerator tokenGenerator;

	public AdministrationUsersBean() {
		this.tokenGenerator = new TokenGenerator();
	}
	////

	/**
	 * @return the userBean
	 */
	public AdministrationUsersBeanLocal getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(AdministrationUsersBeanLocal userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the selectedUser
	 */
	public AdministrationUser getSelectedUser() {
		return selectedUser;
	}

	/**
	 * @param selectedUser
	 *            the selectedUser to set
	 */
	public void setSelectedUser(AdministrationUser selectedUser) {
		this.selectedUser = selectedUser;
	}

	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword
	 *            the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return the confirmPassword
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * @param confirmPassword
	 *            the confirmPassword to set
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail
	 *            the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the locked
	 */
	public boolean isLocked() {
		return locked;
	}

	/**
	 * @param locked
	 *            the locked to set
	 */
	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	/**
	 * @return the adminstrationIdentifier
	 */
	public String getAdminstrationIdentifier() {
		return adminstrationIdentifier;
	}

	/**
	 * @param adminstrationIdentifier
	 *            the adminstrationIdentifier to set
	 */
	public void setAdminstrationIdentifier(String adminstrationIdentifier) {
		this.adminstrationIdentifier = adminstrationIdentifier;
	}

	/**
	 * @return the usersAccess
	 */
	public boolean isUsersAccess() {
		return usersAccess;
	}

	/**
	 * @param usersAccess
	 *            the usersAccess to set
	 */
	public void setUsersAccess(boolean usersAccess) {
		this.usersAccess = usersAccess;
	}

	/**
	 * @return the travelAccess
	 */
	public boolean isTravelAccess() {
		return travelAccess;
	}

	/**
	 * @param travelAccess
	 *            the travelAccess to set
	 */
	public void setTravelAccess(boolean travelAccess) {
		this.travelAccess = travelAccess;
	}

	/**
	 * @return the hotelAccess
	 */
	public boolean isHotelAccess() {
		return hotelAccess;
	}

	/**
	 * @param hotelAccess
	 *            the hotelAccess to set
	 */
	public void setHotelAccess(boolean hotelAccess) {
		this.hotelAccess = hotelAccess;
	}

	/**
	 * @return the transportAccess
	 */
	public boolean isTransportAccess() {
		return transportAccess;
	}

	/**
	 * @param transportAccess
	 *            the transportAccess to set
	 */
	public void setTransportAccess(boolean transportAccess) {
		this.transportAccess = transportAccess;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdministrationUsersBean [userBean=" + userBean + ", selectedUser=" + selectedUser + ", newPassword="
				+ newPassword + ", confirmPassword=" + confirmPassword + ", mail=" + mail + ", locked=" + locked
				+ ", adminstrationIdentifier=" + adminstrationIdentifier + ", usersAccess=" + usersAccess
				+ ", travelAccess=" + travelAccess + ", hotelAccess=" + hotelAccess + ", transportAccess="
				+ transportAccess + "]";
	}

	////
	/**
	 * if allowed disable user account
	 */
	public void disableAccount() {
		this.selectedUser.setDisabled(true);
		userBean.changeAdministrationUser(selectedUser);
		System.err.println("Disable account");
	}

	/**
	 * if both password are same change user password
	 */
	public void changePassword() {
		System.err.println("[Change Password]");
		if (newPassword.equals(confirmPassword)) {
			selectedUser.setHashPassword(tokenGenerator.hashString(newPassword));
			userBean.changeAdministrationUser(selectedUser);
			selectedUser.setHashPassword(newPassword);
			mailBean.sendMail(new AdminstrationUserMail(selectedUser, AdminstrationUserMail.PASSWORD_CHANGE));
		} else {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Hasła nie zgadzają się", null);
			FacesContext.getCurrentInstance().addMessage("auser:users:h_pass", msg);
			FacesContext.getCurrentInstance().addMessage("auser:users:h_conf", msg);
		}
		newPassword = null;
		confirmPassword = null;
		// PrimeFaces.current().executeScript("PF('pass').hide()");
	}

	/**
	 * Save create user instance
	 */
	public void saveUser() {
		System.err.println("[Save User]");
		if (!this.travelAccess && !this.hotelAccess && !this.transportAccess && !this.usersAccess) {
			FacesContext.getCurrentInstance().addMessage("new_auser:id",
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "Nie przydzielono uprawnień", null));
		} else {
			if (newPassword.equals(confirmPassword)) {
				AdministrationUser auser = new AdministrationUser();
				auser.setAdminstrationIdentifier(this.adminstrationIdentifier);
				auser.setMail(this.mail);
				auser.setHashPassword(tokenGenerator.hashString(this.newPassword));
				auser.setLocked(this.locked);
				auser.setUsersAccess(this.usersAccess);
				auser.setTravelAccess(this.travelAccess);
				auser.setHotelAccess(this.hotelAccess);
				auser.setTransportAccess(this.transportAccess);
				try {
					userBean.saveAdministrationUser(auser);
					auser.setHashPassword(this.newPassword);
					mailBean.sendMail(new AdminstrationUserMail(auser, AdminstrationUserMail.NEW_ACCOUNT));
					clear();
				} catch (Exception e) {
					FacesContext.getCurrentInstance().addMessage("new_auser:id", new FacesMessage(
							FacesMessage.SEVERITY_FATAL, "Dane używane przez innego użytkownika", null));
				}

			} else {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Hasła nie zgadzają się", null);
				FacesContext.getCurrentInstance().addMessage("new_auser:pass", msg);
				FacesContext.getCurrentInstance().addMessage("new_auser:conf", msg);
			}
		}

	}

	/**
	 * Get all {@link AdministrationUser}
	 * 
	 * @return adminstration user list
	 */
	public List<AdministrationUser> administrationList() {
		return userBean.getUsers();
	}

	////
	private void clear() {
		selectedUser = null;
		newPassword = null;
		confirmPassword = null;
		mail = null;
		locked = false;
		adminstrationIdentifier = AdministrationUser.PREFIX;
		usersAccess = false;
		travelAccess = false;
		hotelAccess = false;
		transportAccess = false;
	}
	////

}