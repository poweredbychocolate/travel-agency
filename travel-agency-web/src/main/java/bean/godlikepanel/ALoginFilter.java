/**
 * 
 */
package bean.godlikepanel;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Control page access and redirect if necessary
 * 
 * @author Dawid
 * @since 22.06.2018
 * @version 1.0
 *
 */
public class ALoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filter)
			throws IOException, ServletException {

		// /godlikePanelUsers.xhtml
		// /godlikePanelTransport.xhtml
		// /godlikePanelHotels.xhtml
		// /godlikePanelTravels.xhtml
		// /godlikePanelActiveOffer.xhtml
		// /godlikePanelOfferCard.xhtml

		String path = ((HttpServletRequest) request).getServletPath();
		if (path.equals("/godlikePanelUsers.xhtml") || path.equals("/godlikePanelTransport.xhtml")
				|| path.equals("/godlikePanelHotels.xhtml") || path.equals("/godlikePanelTravels.xhtml")
				|| path.equals("/godlikePanelActiveOffer.xhtml") || path.equals("/godlikePanelOfferCard.xhtml")) {

			Boolean adminUserLogged = (Boolean) ((HttpServletRequest) request).getSession()
					.getAttribute("adminUserLogged");
			if (adminUserLogged == null) {
				((HttpServletResponse) response).sendRedirect("godlikePanel.xhtml");
			}
		}
		filter.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
