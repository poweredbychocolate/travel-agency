package bean.godlikepanel;

import java.io.Serializable;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import classic.TokenGenerator;
import enterpriseBean.administration.local.AdministrationUsersBeanLocal;
import entries.adminstration.AdministrationUser;

/**
 * @author Dawid
 * @since 22.04.2018
 * @version 1.0
 *
 */
@Named(value = "godlikeUsers")
@SessionScoped
public class GodlikePanelMenuBean implements Serializable {

	@EJB
	private AdministrationUsersBeanLocal userBean;

	private static final long serialVersionUID = 1L;
	private String loginMail;
	private String loginPass;
	// panel access control
	private AdministrationUser panelUser = null;
	private boolean usersAccess = false;
	private boolean travelAccess = false;
	private boolean hotelAccess = false;
	private boolean transportAccess = false;
	private TokenGenerator tokenGenerator;

	public GodlikePanelMenuBean() {
		this.tokenGenerator = new TokenGenerator();
	}

	public String loginUser() {
		this.panelUser = userBean.getUserIfExist(loginMail, tokenGenerator.hashString(loginPass));
		if (this.panelUser != null) {
			clean();
			// put session parameter, that control access to administration panel
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("adminUserLogged",
					new Boolean(true));
			return switchAccess();
		} else {
			FacesContext.getCurrentInstance().addMessage("alogin:mail",
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "Brak użytkownika o podanych danych ", null));
		}
		return null;
	}

	private void clean() {
		loginMail = null;
		loginPass = null;
	}

	@PreDestroy
	private void removeParamFormSessionMap() {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("adminUserLogged");
	}

	public String logoutUser() {
		this.panelUser = null;
		removeParamFormSessionMap();
		return switchAccess();
	}

	private String switchAccess() {
		// System.err.println(FacesContext.getCurrentInstance().getViewRoot().getViewId());
		if (panelUser == null) {
			usersAccess = false;
			travelAccess = false;
			hotelAccess = false;
			transportAccess = false;
			return "godlikePanel?faces-redirect=true";
		} else {
			usersAccess = panelUser.isUsersAccess();
			travelAccess = panelUser.isTravelAccess();
			hotelAccess = panelUser.isHotelAccess();
			transportAccess = panelUser.isTransportAccess();
			if (travelAccess) {
				return "godlikePanelTravels?faces-redirect=true";
			}
			if (hotelAccess) {
				return "godlikePanelHotels?faces-redirect=true";
			}
			if (transportAccess) {
				return "godlikePanelTransport?faces-redirect=true";
			}
			if (usersAccess) {
				return "godlikePanelUsers?faces-redirect=true";
			}
		}
		return null;
	}

	/**
	 * @return the loginMail
	 */
	public String getLoginMail() {
		return loginMail;
	}

	/**
	 * @param loginMail
	 *            the loginMail to set
	 */
	public void setLoginMail(String loginMail) {
		this.loginMail = loginMail;
	}

	/**
	 * @return the loginPass
	 */
	public String getLoginPass() {
		return loginPass;
	}

	/**
	 * @param loginPass
	 *            the loginPass to set
	 */
	public void setLoginPass(String loginPass) {
		this.loginPass = loginPass;
	}

	/**
	 * @return the usersAccess
	 */
	public boolean hasUsersAccess() {
		return usersAccess;
	}

	/**
	 * @return the travelAccess
	 */
	public boolean hasTravelAccess() {
		return travelAccess;
	}

	/**
	 * @return the hotelAccess
	 */
	public boolean hasHotelAccess() {
		return hotelAccess;
	}

	/**
	 * @return the transportAccess
	 */
	public boolean hasTransportAccess() {
		return transportAccess;
	}

	/**
	 * @return the user logged
	 */
	public boolean userIsLogged() {
		if (panelUser != null)
			return true;
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GodlikePanelBean [userBean=" + userBean + ", loginMail=" + loginMail + ", loginPass=" + loginPass
				+ ", panelUser=" + panelUser + ", usersAccess=" + usersAccess + ", travelAccess=" + travelAccess
				+ ", hotelAccess=" + hotelAccess + ", transportAccess=" + transportAccess + "]";
	}
}
