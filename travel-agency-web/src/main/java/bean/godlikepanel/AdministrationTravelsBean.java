
package bean.godlikepanel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.Part;

import enterpriseBean.administration.local.TravelFactoryBeanLocal;
import entries.Address;
import entries.InternetMedia;
import entries.hotels.HotelReservation;
import entries.planes.PlaneTicket;
import entries.travel.TravelOffer;

/**
 * 
 * @author Dawid
 * @version 1.0
 * @since 25.04.2018
 *
 */
@Named
@SessionScoped
public class AdministrationTravelsBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private TravelFactoryBeanLocal travelFactoryBean;
	private TravelOffer selectedTravel;
	private TravelOffer finalizeTravel;

	private String title;
	private String city;
	private String country;
	private Date startDate = new Date();
	private String description;
	private Integer commission;

	private List<HotelReservation> foundHotelReservations = null;
	private List<HotelReservation> selectedHotelReservations = null;
	private String searchHotel = "";

	private List<PlaneTicket> foundPlaneTickets = null;
	private List<PlaneTicket> selectedPlaneTickets = null;
	private String searchPlane = "";

	private String imageName;
	private Part imageFile;

	private String createdOfferFilter = null;
	private String activeOfferFilter = null;
	private String readyOfferFilter = null;
	private String hotelOfferFilter = null;
	private String planeOfferFilter = null;

	public AdministrationTravelsBean() {
	}

	/**
	 * @return the selectedTravel
	 */
	public TravelOffer getSelectedTravel() {
		return selectedTravel;
	}

	/**
	 * @param selectedTravel
	 *            the selectedTravel to set
	 */
	public void setSelectedTravel(TravelOffer selectedTravel) {
		this.selectedTravel = selectedTravel;
	}

	/**
	 * 
	 * @return redirect to page
	 */
	public String openCard() {
		return "godlikePanelOfferCard?faces-redirect=true";
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the price
	 */
	public Integer getCommission() {
		return commission;
	}

	/**
	 * @param commission
	 *            the commission to set
	 */
	public void setCommission(Integer commission) {
		this.commission = commission;
	}

	/**
	 * @return the foundHotelReservations
	 */
	public List<HotelReservation> getFoundHotelReservations() {
		return foundHotelReservations;
	}

	/**
	 * @param foundHotelReservations
	 *            the foundHotelReservations to set
	 */
	public void setFoundHotelReservations(List<HotelReservation> foundHotelReservations) {
		this.foundHotelReservations = foundHotelReservations;
	}

	/**
	 * @return the selectedHotelReservations
	 */
	public List<HotelReservation> getSelectedHotelReservations() {
		return selectedHotelReservations;
	}

	/**
	 * @param selectedHotelReservations
	 *            the selectedHotelReservations to set
	 */
	public void setSelectedHotelReservations(List<HotelReservation> selectedHotelReservations) {
		this.selectedHotelReservations = selectedHotelReservations;
	}

	/**
	 * @return the foundPlaneTickets
	 */
	public List<PlaneTicket> getFoundPlaneTickets() {
		return foundPlaneTickets;
	}

	/**
	 * @param foundPlaneTickets
	 *            the foundPlaneTickets to set
	 */
	public void setFoundPlaneTickets(List<PlaneTicket> foundPlaneTickets) {
		this.foundPlaneTickets = foundPlaneTickets;
	}

	/**
	 * @return the selectedPlaneTickets
	 */
	public List<PlaneTicket> getSelectedPlaneTickets() {
		return selectedPlaneTickets;
	}

	/**
	 * @param selectedPlaneTickets
	 *            the selectedPlaneTickets to set
	 */
	public void setSelectedPlaneTickets(List<PlaneTicket> selectedPlaneTickets) {
		this.selectedPlaneTickets = selectedPlaneTickets;
	}

	/**
	 * @return the searchHotel
	 */
	public String getSearchHotel() {
		return searchHotel;
	}

	/**
	 * @param searchHotel
	 *            the searchHotel to set
	 */
	public void setSearchHotel(String searchHotel) {
		this.searchHotel = searchHotel;
	}

	/**
	 * @return the searchPlane
	 */
	public String getSearchPlane() {
		return searchPlane;
	}

	/**
	 * @param searchPlane
	 *            the searchPlane to set
	 */
	public void setSearchPlane(String searchPlane) {
		this.searchPlane = searchPlane;
	}

	/**
	 * @return the imageFile
	 */
	public Part getImageFile() {
		return imageFile;
	}

	/**
	 * @param imageFile
	 *            the imageFile to set
	 */
	public void setImageFile(Part imageFile) {
		this.imageFile = imageFile;
	}

	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}

	/**
	 * @param imageName
	 *            the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	/**
	 * Remove saved {@link TravelOffer}
	 */
	public void removeOffer() {
		travelFactoryBean.removeOffer(selectedTravel);

	}

	/**
	 * {@link TravelOffer} list that wait for add hotels
	 * 
	 * @return the travel Offer List
	 */
	public List<TravelOffer> travelList() {
		return travelFactoryBean.getOfferByStatus(TravelOffer.TYPE_CREATE);
	}

	/**
	 * {@link TravelOffer} list that wait for add hotels
	 * 
	 * @return the travel Offer List
	 */
	public List<TravelOffer> travelListForPrepareHotels() {
		return travelFactoryBean.getFullyOfferByCriteria(TravelOffer.TYPE_CREATE, hotelOfferFilter);

	}

	/**
	 * {@link TravelOffer} list that wait for add transport
	 * 
	 * @return the travel Offer List
	 */
	public List<TravelOffer> travelListForPrepareTransport() {
		return travelFactoryBean.getFullyOfferByCriteria(TravelOffer.TYPE_PREPARED_HOTELS, planeOfferFilter);
	}

	/**
	 * {@link TravelOffer} list that wait for add transport
	 * 
	 * @return the travel Offer List
	 */
	public List<TravelOffer> travelListInProgress() {
		return travelFactoryBean.getOfferInProgress(createdOfferFilter);
	}

	/**
	 * {@link TravelOffer} list that wait for add transport
	 * 
	 * @return the travel Offer List
	 */
	public List<TravelOffer> readyTravelList() {
		return travelFactoryBean.getFullyOfferByCriteria(TravelOffer.TYPE_PREPARED_TRANSPORT, readyOfferFilter);
	}

	public List<TravelOffer> activeTravelList() {
		List<TravelOffer> list = travelFactoryBean.getFullyOfferByCriteria(TravelOffer.TYPE_ACTIVE, activeOfferFilter);
		// cityFilter = null;
		return list;

	}

	/**
	 * Crate new {@link TravelOffer} as ready to prepared
	 */
	public void addOfferToPrepare() {
		TravelOffer travel = new TravelOffer();
		travel.setType(TravelOffer.TYPE_CREATE);
		travel.setTitle(title);
		Address address = new Address();
		address.setCountry(country);
		address.setCity(city);
		travel.setAddress(address);
		travel.setStartDate(startDate);
		travelFactoryBean.saveOffer(travel);
		title = null;
		country = null;
		city = null;
		address = null;
		startDate = new Date();
	}

	/**
	 * If hotels list has more than zero {@link HotelReservation} mark
	 * {@link TravelOffer} as consist Hotels proposition
	 */
	public void changeOfferToPreparedHotels() {
		if (selectedTravel.getHotels().size() > 0) {
			selectedTravel.setType(TravelOffer.TYPE_PREPARED_HOTELS);
			travelFactoryBean.saveOffer(selectedTravel);
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nie dodano hoteli !", null));
		}
	}

	/**
	 * If planeTickets list has more than zero {@link PlaneTicket} mark
	 * {@link TravelOffer} as consist Planes proposition
	 */
	public void changeOfferToPreparedTrasport() {
		if (selectedTravel.getPlanes().size() > 0) {
			selectedTravel.setType(TravelOffer.TYPE_PREPARED_TRANSPORT);
			travelFactoryBean.saveOffer(selectedTravel);
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nie dodano samolotów !", null));
		}
	}

	// > For Hotels
	/**
	 * Load to foundHotels {@link HotelReservation} list that city correspond to
	 * value entered in search field
	 */
	public void findHotel() {
		foundHotelReservations = travelFactoryBean.getHotelsByCriteria(searchHotel, HotelReservation.TYPE_EXTERNAL);
	}

	/**
	 * Linked selected hotels with {@link TravelOffer}
	 */
	public void addSelectedHotels() {
		for (HotelReservation hotelReservation : selectedHotelReservations) {
			hotelReservation.setType(HotelReservation.TYPE_TRAVEL);
		}
		selectedTravel.getHotels().addAll(selectedHotelReservations);
		searchHotel = "";
		selectedHotelReservations = null;
		foundHotelReservations = null;
		travelFactoryBean.saveOffer(selectedTravel);
	}

	// > For planes
	/**
	 * Load to foundPlanes {@link PlaneTicket} list that fly destination correspond
	 * to value entered in search field
	 */
	public void findPlanes() {
		foundPlaneTickets = travelFactoryBean.getPlaneByCriteria(searchPlane);
	}

	/**
	 * Linked selected hotels with {@link TravelOffer}
	 */
	public void addSelectedPlanes() {

		selectedTravel.getPlanes().addAll(selectedPlaneTickets);
		searchPlane = "";
		selectedPlaneTickets = null;
		foundPlaneTickets = null;
		travelFactoryBean.saveOffer(selectedTravel);
	}

	// >For finalize offer
	/**
	 * @return the finalizeTravel
	 */
	public TravelOffer getFinalizeTravel() {
		return finalizeTravel;
	}

	/**
	 * @param finalizeTravel
	 *            the finalizeTravel to set
	 */
	public void setFinalizeTravel(TravelOffer finalizeTravel) {
		System.err.println("Finalize");
		this.finalizeTravel = finalizeTravel;
		title = finalizeTravel.getTitle();
		city = finalizeTravel.getAddress().getCity();
		startDate = finalizeTravel.getStartDate();
		description = finalizeTravel.getDescription();
		commission = finalizeTravel.getOfficeCommission();
		foundHotelReservations = finalizeTravel.getHotelsList();
		foundPlaneTickets = finalizeTravel.getPlanesList();
		selectedHotelReservations = null;
		selectedPlaneTickets = null;
	}

	public void changeOffer() {
		System.err.println("Change Offer");

		if (selectedHotelReservations != null && !selectedHotelReservations.isEmpty()) {

			foundHotelReservations = selectedHotelReservations;
			finalizeTravel.setHotels(selectedHotelReservations);
		}
		if (selectedPlaneTickets != null && !selectedPlaneTickets.isEmpty()) {

			foundPlaneTickets = selectedPlaneTickets;
			finalizeTravel.setPlanes(selectedPlaneTickets);
		}
		saveImage();
		// travelFactoryBean.saveOffer(finalizeTravel);
		TravelOffer tmp = travelFactoryBean.refreshOffer(finalizeTravel);
		finalizeTravel = tmp;
		// finalizeTravel = travelFactoryBean.refreshOffer(finalizeTravel);

	}

	public void changeAcriveOffer() {
		PlaneTicket planeTicket = finalizeTravel.getPlanesList().stream()
				.min(Comparator.comparing(PlaneTicket::getTicketPrice)).get();
		HotelReservation hotelReservation = finalizeTravel.getHotelsList().stream()
				.min(Comparator.comparing(HotelReservation::getPrice)).get();
		finalizeTravel.setMinPrice(finalizeTravel.getOfficeCommission() + hotelReservation.getPrice()
				+ planeTicket.getTicketPrice() * hotelReservation.getPersonInRoom());
		changeOffer();
	}

	public String activateOffer() {
		saveImage();
		this.finalizeTravel.setType(TravelOffer.TYPE_READY);
		// travelFactoryBean.saveOffer(finalizeTravel);
		finalizeTravel = travelFactoryBean.refreshOffer(finalizeTravel);
		foundHotelReservations = null;
		selectedHotelReservations = null;
		foundPlaneTickets = null;
		selectedPlaneTickets = null;
		return "godlikePanelTravels?faces-redirect=true";
	}

	public String disableOffer() {
		this.finalizeTravel.setType(TravelOffer.TYPE_ARCHIVE);
		// travelFactoryBean.saveOffer(finalizeTravel);
		finalizeTravel = travelFactoryBean.refreshOffer(finalizeTravel);
		foundHotelReservations = null;
		selectedHotelReservations = null;
		foundPlaneTickets = null;
		selectedPlaneTickets = null;
		return "godlikePanelTravels?faces-redirect=true";
	}

	public void onTitleChange() {
		finalizeTravel.setTitle(title);
	}

	public void onStartDateChange() {
		finalizeTravel.setStartDate(startDate);
	}

	public void onDescriptionChange() {
		finalizeTravel.setDescription(description);
	}

	public void onPriceChange() {
		finalizeTravel.setOfficeCommission(commission);
	}

	// >For images
	private void saveImage() {
		System.err.println("SaveImage");
		if (imageFile != null) {

			InternetMedia internetMedia = new InternetMedia();
			byte[] binImage = null;
			try {
				InputStream inputStream = imageFile.getInputStream();
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				//// copy input stream to output stream
				binImage = new byte[inputStream.available()];
				int size;
				while ((size = inputStream.read(binImage)) != -1) {
					outputStream.write(binImage, 0, size);
				}
				binImage = outputStream.toByteArray();
				inputStream.close();
				outputStream.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
			internetMedia.setMediaBytes(binImage);
			internetMedia.setName(imageName);
			internetMedia.setCategory(InternetMedia.CATEGORY_TRAVELS);
			internetMedia.setType(imageFile.getContentType());
			finalizeTravel.setImage(internetMedia);
			// travelFactoryBean.saveOffer(finalizeTravel);
			// finalizeTravel = travelFactoryBean.refreshOffer(finalizeTravel);
			// System.err.println(finalizeTravel.toString());
			imageFile = null;
			imageName = null;
		}
	}

	public String openActiveTravel() {
		return "godlikePanelActiveOffer?faces-redirect=true";
	}

	public HashMap<String, String> activeOfferCitiesList() {
		HashMap<String, String> hashMap = new LinkedHashMap<>();
		hashMap.put("Wszystkie", null);
		hashMap.putAll(travelFactoryBean.travelCitiesByCriteria(TravelOffer.TYPE_ACTIVE).stream()
				.collect(Collectors.toMap(s -> s, s -> s)));
		return hashMap;

	}

	public HashMap<String, String> readyOfferCitiesList() {
		HashMap<String, String> hashMap = new LinkedHashMap<>();
		hashMap.put("Wszystkie", null);
		hashMap.putAll(travelFactoryBean.travelCitiesByCriteria(TravelOffer.TYPE_PREPARED_TRANSPORT).stream()
				.collect(Collectors.toMap(s -> s, s -> s)));
		return hashMap;

	}

	public HashMap<String, String> createdOfferCitiesList() {
		HashMap<String, String> hashMap = new LinkedHashMap<>();
		hashMap.put("Wszystkie", null);
		hashMap.putAll(
				travelFactoryBean.travelCitiesByCriteria(TravelOffer.TYPE_CREATE, TravelOffer.TYPE_PREPARED_HOTELS)
						.stream().collect(Collectors.toMap(s -> s, s -> s)));
		return hashMap;

	}

	public HashMap<String, String> hotelOfferCitiesList() {
		HashMap<String, String> hashMap = new LinkedHashMap<>();
		hashMap.put("Wszystkie", null);
		hashMap.putAll(travelFactoryBean.travelCitiesByCriteria(TravelOffer.TYPE_CREATE).stream()
				.collect(Collectors.toMap(s -> s, s -> s)));
		return hashMap;

	}

	public HashMap<String, String> planeOfferCitiesList() {
		HashMap<String, String> hashMap = new LinkedHashMap<>();
		hashMap.put("Wszystkie", null);
		hashMap.putAll(travelFactoryBean.travelCitiesByCriteria(TravelOffer.TYPE_PREPARED_HOTELS).stream()
				.collect(Collectors.toMap(s -> s, s -> s)));
		return hashMap;

	}

	/**
	 * @return the readyOfferFilter
	 */
	public String getReadyOfferFilter() {
		return readyOfferFilter;
	}

	/**
	 * @param readyOfferFilter
	 *            the readyOfferFilter to set
	 */
	public void setReadyOfferFilter(String readyOfferFilter) {
		this.readyOfferFilter = readyOfferFilter;
	}

	/**
	 * @return the activeOfferFilter
	 */
	public String getActiveOfferFilter() {
		return activeOfferFilter;
	}

	/**
	 * @param activeOfferFilter
	 *            the activeOfferFilter to set
	 */
	public void setActiveOfferFilter(String activeOfferFilter) {
		this.activeOfferFilter = activeOfferFilter;
	}

	/**
	 * @return the createdOfferFilter
	 */
	public String getCreatedOfferFilter() {
		return createdOfferFilter;
	}

	/**
	 * @param createdOfferFilter
	 *            the createdOfferFilter to set
	 */
	public void setCreatedOfferFilter(String createdOfferFilter) {
		this.createdOfferFilter = createdOfferFilter;
	}

	/**
	 * @return the hotelOfferFilter
	 */
	public String getHotelOfferFilter() {
		return hotelOfferFilter;
	}

	/**
	 * @param hotelOfferFilter
	 *            the hotelOfferFilter to set
	 */
	public void setHotelOfferFilter(String hotelOfferFilter) {
		this.hotelOfferFilter = hotelOfferFilter;
	}

	/**
	 * @return the planeOfferFilter
	 */
	public String getPlaneOfferFilter() {
		return planeOfferFilter;
	}

	/**
	 * @param planeOfferFilter
	 *            the planeOfferFilter to set
	 */
	public void setPlaneOfferFilter(String planeOfferFilter) {
		this.planeOfferFilter = planeOfferFilter;
	}
}
