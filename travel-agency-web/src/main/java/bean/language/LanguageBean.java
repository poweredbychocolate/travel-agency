package bean.language;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;

/**
 * Set and allow change prefer language if exist on predefined list
 * 
 * @author Dawid
 * @version 1.0
 */
@Named
@SessionScoped
public class LanguageBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String language;
	/**
	 * predefined language list
	 */
	private static Map<String, String> languages;

	public LanguageBean() {
	}

	@PostConstruct
	/**
	 * Set user locale or set default specified in faces-config.xml, if user locale
	 * is not supported
	 */
	private void checkAndSetLanguage() {
		// init supqorted language list
		languages = new HashMap<String, String>();
		languages.put("English", "en_GB");
		languages.put("Polski", "pl_PL");
		// check if supported
		if (languages.containsValue(FacesContext.getCurrentInstance().getViewRoot().getLocale().toString()))
			// set user locale
			this.language = FacesContext.getCurrentInstance().getViewRoot().getLocale().toString();
		else
			// set locale specified as default
			this.language = FacesContext.getCurrentInstance().getApplication().getDefaultLocale().toString();

	}

	/**
	 * Return all supported language
	 * 
	 * @return languages map
	 */
	public Map<String, String> getLanguages() {
		return languages;
	}

	/**
	 * Return current language
	 * 
	 * @return language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Set selected language
	 * 
	 * @param language
	 *            language locale
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * Detect change language by user
	 * 
	 * @param e
	 *            {@link ValueChangeEvent}
	 */
	public void onLanguageChanged(ValueChangeEvent e) {
		String newLocaleValue = e.getNewValue().toString();
		if (languages.containsValue(newLocaleValue)) {
			language = newLocaleValue;
			FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(newLocaleValue));
		}
	}
}