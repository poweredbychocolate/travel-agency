package bean;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

import entries.client.ClientReview;
import entries.client.Person;
import entries.hotels.HotelReservation;
import entries.planes.PlaneTicket;
import entries.travel.ClientTravelCard;
import entries.travel.TravelOffer;

import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

import classic.PDF;
import enterpriseBean.client.local.TravelOfferBeanLocal;

/**
 * @author Dawid
 * @since 26.04.2018
 * @version 1.0
 */
@Named
@SessionScoped
public class ClientTravelBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private TravelOfferBeanLocal travelOfferBean;
	@Inject
	private ClientAccountBean clientAccountBean;

	private TravelOffer selectedTravelOffer = null;
	private ClientTravelCard selectedClientTravelCard = null;

	private PlaneTicket selectedPlane = null;
	private HotelReservation selectedHotel = null;

	private List<Person> accompanyingPersons;

	private List<Person> personHistory;

	private Integer selectedPersonId;
	private Integer positionIndex;

	private String rewiewContent;
	private String rewiewTitle;
	private Integer reviewRatng = 5;

	/**
	 * 
	 */
	public ClientTravelBean() {

	}

	/**
	 * @return the selectedPlane
	 */
	public PlaneTicket getSelectedPlane() {
		return selectedPlane;
	}

	/**
	 * @param selectedPlane
	 *            the selectedPlane to set
	 */
	public void setSelectedPlane(PlaneTicket selectedPlane) {
		this.selectedPlane = selectedPlane;
	}

	/**
	 * @return the selectedHotel
	 */
	public HotelReservation getSelectedHotel() {
		return selectedHotel;
	}

	/**
	 * @param selectedHotel
	 *            the selectedHotel to set
	 */
	public void setSelectedHotel(HotelReservation selectedHotel) {
		this.selectedHotel = selectedHotel;
	}

	public TravelOffer getSelectedTravelOffer() {
		return selectedTravelOffer;
	}

	public void setSelectedTravelOffer(TravelOffer selectedTravelOffer) {
		this.selectedTravelOffer = selectedTravelOffer;
	}

	/**
	 * @return the selectedClientTravelCard
	 */
	public ClientTravelCard getSelectedClientTravelCard() {
		return selectedClientTravelCard;
	}

	/**
	 * @param selectedClientTravelCard
	 *            the selectedClientTravelCard to set
	 */
	public void setSelectedClientTravelCard(ClientTravelCard selectedClientTravelCard) {
		this.selectedClientTravelCard = selectedClientTravelCard;
		initReview();
	}

	private void initReview() {
		if (this.selectedClientTravelCard.getClientReview() == null) {
			this.rewiewTitle = this.selectedClientTravelCard.getTravelOffer().getTitle();
			this.reviewRatng = 5;
		} else {
			rewiewContent = this.selectedClientTravelCard.getClientReview().getRewiew();
			rewiewTitle = this.selectedClientTravelCard.getClientReview().getTitle();
			reviewRatng = this.selectedClientTravelCard.getClientReview().getRating();
		}
	}

	/**
	 * @return the travelOfferBean
	 */
	public TravelOfferBeanLocal getTravelOfferBean() {
		return travelOfferBean;
	}

	/**
	 * @return the userAccountBean
	 */
	public ClientAccountBean getClientAccountBean() {
		return clientAccountBean;
	}

	/**
	 * @param clientAccountBean
	 *            the clientAccountBean to set
	 */
	public void setClientAccountBean(ClientAccountBean clientAccountBean) {
		this.clientAccountBean = clientAccountBean;
	}

	public List<TravelOffer> getHottestOffer() {
		return travelOfferBean.getNewestOffer(5);
	}

	public void onHotelSelect(SelectEvent event) {
		System.err.println(((HotelReservation) event.getObject()).toString());
	}

	public void onPlaneSelect(SelectEvent event) {
		System.err.println(((PlaneTicket) event.getObject()).toString());
	}

	public String openClientOffer() {
		selectedClientTravelCard = travelOfferBean.findFullyClientTravelCard(selectedClientTravelCard.getId());
		return "travelCardDetails?faces-redirect=true";
	}

	public String buyTravel() {
		System.err.println("[Buy Travel]");
		if (clientAccountBean.getClient() == null) {
			// open NotificationBar with user no logged message and hide it on current
			// timeout
			PrimeFaces.current().executeScript("PF('noLogged').show();setTimeout(\"PF('noLogged').hide()\", 4000);");
			return null;
		} else {
			int count = this.selectedHotel.getPersonInRoom();
			accompanyingPersons = new ArrayList<>(--count);
			Person clientPerson = clientAccountBean.getClient().getClientData();
			for (int i = 0; i < count; i++) {
				Person person = new Person();
				person.setCountry(clientPerson.getCountry());
				person.setPlace(clientPerson.getPlace());
				person.setStreet(clientPerson.getStreet());
				person.setHouseNumber(clientPerson.getHouseNumber());
				person.setApartmentNumber(clientPerson.getApartmentNumber());
				person.setPostalCode(clientPerson.getPostalCode());
				accompanyingPersons.add(person);
			}
			personHistory = travelOfferBean.clientPersonsList(clientAccountBean.getClient().getId());
			return "buyTravel?faces-redirect=true";
		}
	}

	public long dayCount() {
		long difference = selectedPlane.getReturnDate().getTime() - selectedPlane.getDepartureDate().getTime();
		return TimeUnit.MILLISECONDS.toDays(difference);
	}

	public String finalizeOffer() {
		System.err.println("Finalize offer");

		ClientTravelCard clientTravelCard = new ClientTravelCard();
		clientTravelCard.setHotelReservation(selectedHotel);
		clientTravelCard.setPlaneTicket(selectedPlane);
		clientTravelCard.setTravelOffer(selectedTravelOffer);
		clientTravelCard.setAccompanyingPersons(accompanyingPersons);
		clientTravelCard.setClientAccount(clientAccountBean.getClient());

		selectedHotel.setSoldRoomCount(selectedHotel.getSoldRoomCount() + 1);
		selectedPlane.setSoldTicket(selectedPlane.getSoldTicket() + selectedHotel.getPersonInRoom());
		travelOfferBean.saveClientTravelCard(clientTravelCard);
		this.selectedClientTravelCard = clientTravelCard;
		initReview();
		return "travelCardDetails?faces-redirect=true";

	}

	public List<ClientTravelCard> userOffer() {
		return travelOfferBean.getUserOffer(clientAccountBean.getClient());
		// return userAccountBean.getClient().getClientTravelCards();

	}

	/**
	 * @return the accompanyingPersons
	 */
	public List<Person> getAccompanyingPersons() {
		return accompanyingPersons;
	}

	/**
	 * @param accompanyingPersons
	 *            the accompanyingPersons to set
	 */
	public void setAccompanyingPersons(List<Person> accompanyingPersons) {
		this.accompanyingPersons = accompanyingPersons;
	}

	/**
	 * @return the personHistory
	 */
	public List<Person> getPersonHistory() {
		return personHistory;
	}

	/**
	 * @param personHistory
	 *            the personHistory to set
	 */
	public void setPersonHistory(List<Person> personHistory) {
		this.personHistory = personHistory;
	}

	public void onPersonChanged() {
		System.err.println("[On Person Changed]" + positionIndex + " : " + selectedPersonId);

	}

	/**
	 * @return the selectedPersonIndex
	 */
	public Integer getSelectedPersonId() {
		return selectedPersonId;
	}

	/**
	 * @param selectedPersonId
	 *            the selectedPersonId to set
	 */
	public void setSelectedPersonId(Integer selectedPersonId) {
		this.selectedPersonId = selectedPersonId;
	}

	/**
	 * @return the positionIndex
	 */
	public Integer getPositionIndex() {
		return positionIndex;
	}

	/**
	 * @param positionIndex
	 *            the positionIndex to set
	 */
	public void setPositionIndex(Integer positionIndex) {
		this.positionIndex = positionIndex;
	}

	public void openPDF() {
		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			PDF file = new PDF(selectedClientTravelCard);
			byte[] fileBytes = file.bulid();
			response.reset();
			response.setHeader("Content-Type", "application/pdf");
			response.setHeader("Content-Length", String.valueOf(fileBytes.length));
			response.setHeader("Content-Disposition",
					"inline; filename=\"" + selectedClientTravelCard.getTravelOffer().getTitle() + ".pdf" + "\"");
			BufferedOutputStream output;
			output = new BufferedOutputStream(response.getOutputStream());
			output.write(fileBytes);
			output.flush();
			output.close();
			FacesContext.getCurrentInstance().responseComplete();
		} catch (IOException e) {
			System.err.println("[openPDF Fail]");
		}

	}

	/**
	 * @return the rewiewContent
	 */
	public String getRewiewContent() {
		return rewiewContent;
	}

	/**
	 * @param rewiewContent
	 *            the rewiewContent to set
	 */
	public void setRewiewContent(String rewiewContent) {
		this.rewiewContent = rewiewContent;
	}

	/**
	 * @return the rewiewTitle
	 */
	public String getRewiewTitle() {
		return rewiewTitle;
	}

	/**
	 * @param rewiewTitle
	 *            the rewiewTitle to set
	 */
	public void setRewiewTitle(String rewiewTitle) {
		this.rewiewTitle = rewiewTitle;
	}

	/**
	 * @return the reviewRatng
	 */
	public Integer getReviewRatng() {
		return reviewRatng;
	}

	/**
	 * @param reviewRatng
	 *            the reviewRatng to set
	 */
	public void setReviewRatng(Integer reviewRatng) {
		this.reviewRatng = reviewRatng;
	}

	public void addReview() {
		ClientReview clientReview = new ClientReview();
		clientReview.setRating(reviewRatng);
		clientReview.setTitle(rewiewTitle);
		clientReview.setRewiew(rewiewContent);
		selectedClientTravelCard.setClientReview(clientReview);
		selectedClientTravelCard.getTravelOffer().getClientReviews().add(clientReview);
		selectedClientTravelCard = travelOfferBean.saveClientTravelCard(selectedClientTravelCard);

	}

}
