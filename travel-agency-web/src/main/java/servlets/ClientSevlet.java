package servlets;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import enterpriseBean.client.local.ClientUserBeanLocal;
import entries.client.ActivationToken;
import entries.client.ClientAccount;

/**
 * Servlet implementation class ClientSevlet. Active user account from
 * activation link
 * 
 * @author Dawid
 * @since 20.06.2018
 * @version 1.0
 */
@WebServlet("/Client")
public class ClientSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private ClientUserBeanLocal clientUserBean;

	/**
	 * Default constructor.
	 */
	public ClientSevlet() {

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected synchronized void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		String url = request.getRequestURL().toString();
		String path = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath();
		String link = "<a href=\"" + path + "\">" + path + "</a>";
		System.err.println(path);
		try {
			Integer tokenId = Integer.parseInt(request.getParameter("user"));
			String token = request.getParameter("key");
			ActivationToken activationToken = clientUserBean.findToken(tokenId);
			if (activationToken.getToken().equals(token)) {
				ClientAccount clientAccount = activationToken.getClientAccount();
				clientAccount.setType(ClientAccount.TYPE_ACTIVE);
				clientUserBean.saveClientAccount(clientAccount);
				clientUserBean.removeActivationToken(activationToken);
				String msg = "Twoje konto zostało aktywowane";
				response.getWriter().append(htmlMSG(msg + "<br/><br/>" + link));
				return;
			}

		} catch (Exception e) {

		}
		response.getWriter().append(htmlMSG("Token nie aktywny" + "<br/><br/>" + link));

	}

	private String htmlMSG(String content) {
		return "<html lang=\"pl_PL\"><head><meta charset=\"UTF-8\"/><title>Biuro podróży</title></head><body>" + content
				+ "</body></html>";
	}

}
