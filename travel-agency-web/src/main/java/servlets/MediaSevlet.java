package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import enterpriseBean.dataProcessing.local.InternetMediaForServletBeanLocal;
import entries.InternetMedia;;

/**
 * Servlet implementation class MediaSevlet. Return image from database as web
 * resource
 * 
 * @author Dawid
 * @since 19.04.2018
 * @version 1.0
 */
@WebServlet("/Media")
public class MediaSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	@EJB
	private InternetMediaForServletBeanLocal mediaBean;

	public MediaSevlet() {
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected synchronized void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
					Integer media = Integer.parseInt(request.getParameter("media"));
			InternetMedia internetMedia = mediaBean.find(media);
			response.reset();
			response.setContentType(internetMedia.getType());
			response.setContentLength(internetMedia.getMediaBytes().length);
			response.getOutputStream().write(internetMedia.getMediaBytes());
			response.flushBuffer();
			return;
		
		} catch (Exception e) {
			if (!response.isCommitted())
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		// by default return page not found
		if (!response.isCommitted())
			response.sendError(HttpServletResponse.SC_NOT_FOUND);

	}

}
