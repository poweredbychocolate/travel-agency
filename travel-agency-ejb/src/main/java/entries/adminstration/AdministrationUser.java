package entries.adminstration;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: User
 * 
 * @author Dawid
 * @since 21.04.2018
 * @version 1.0
 */
@Entity
@Table(name = "Administration_User")
public class AdministrationUser implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String PREFIX = "ADMINISTRATION-";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(nullable = false, unique = true)
	private String adminstrationIdentifier;

	@Column(unique = true, nullable = false)
	private String mail;
	@Column(nullable = false)
	private String hashPassword;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date registerDate = new Date();
	@Column(nullable = false)
	private boolean disabled = false;
	@Column(nullable = false)
	private boolean locked = false;

	@Column(nullable = false)
	private boolean usersAccess = false;
	@Column(nullable = false)
	private boolean travelAccess = false;
	@Column(nullable = false)
	private boolean hotelAccess = false;
	@Column(nullable = false)
	private boolean transportAccess = false;

	public AdministrationUser() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the adminstrationIdentifier
	 */
	public String getAdminstrationIdentifier() {
		return adminstrationIdentifier;
	}

	/**
	 * @param adminstrationIdentifier
	 *            the adminstrationIdentifier to set
	 */
	public void setAdminstrationIdentifier(String adminstrationIdentifier) {
		this.adminstrationIdentifier = adminstrationIdentifier;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail
	 *            the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the hashPassword
	 */
	public String getHashPassword() {
		return hashPassword;
	}

	/**
	 * @param hashPassword
	 *            the hashPassword to set
	 */
	public void setHashPassword(String hashPassword) {
		this.hashPassword = hashPassword;
	}

	/**
	 * @return the registerDate
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * @param registerDate
	 *            the registerDate to set
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * @return the disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * @param disabled
	 *            the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * @return the locked
	 */
	public boolean isLocked() {
		return locked;
	}

	/**
	 * @param locked
	 *            the locked to set
	 */
	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	/**
	 * @return the usersAccess
	 */
	public boolean isUsersAccess() {
		return usersAccess;
	}

	/**
	 * @param usersAccess
	 *            the usersAccess to set
	 */
	public void setUsersAccess(boolean usersAccess) {
		this.usersAccess = usersAccess;
	}

	/**
	 * @return the travelAccess
	 */
	public boolean isTravelAccess() {
		return travelAccess;
	}

	/**
	 * @param travelAccess
	 *            the travelAccess to set
	 */
	public void setTravelAccess(boolean travelAccess) {
		this.travelAccess = travelAccess;
	}

	/**
	 * @return the hotelAccess
	 */
	public boolean isHotelAccess() {
		return hotelAccess;
	}

	/**
	 * @param hotelAccess
	 *            the hotelAccess to set
	 */
	public void setHotelAccess(boolean hotelAccess) {
		this.hotelAccess = hotelAccess;
	}

	/**
	 * @return the transportAccess
	 */
	public boolean isTransportAccess() {
		return transportAccess;
	}

	/**
	 * @param transportAccess
	 *            the transportAccess to set
	 */
	public void setTransportAccess(boolean transportAccess) {
		this.transportAccess = transportAccess;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdministrationUser [id=" + id + ", adminstrationIdentifier=" + adminstrationIdentifier + ", mail="
				+ mail + ", hashPassword=" + hashPassword + ", registerDate=" + registerDate + ", disabled=" + disabled
				+ ", locked=" + locked + ", usersAccess=" + usersAccess + ", travelAccess=" + travelAccess
				+ ", hotelAccess=" + hotelAccess + ", transportAccess=" + transportAccess + "]";
	}

}
