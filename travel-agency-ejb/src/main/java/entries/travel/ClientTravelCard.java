package entries.travel;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import entries.client.ClientAccount;
import entries.client.ClientReview;
import entries.client.Person;
import entries.hotels.HotelReservation;
import entries.planes.PlaneTicket;

/**
 * Entity implementation class for Entity: ClientTravelOffer
 * 
 * @author Dawid
 * @since 05.06.2018
 * @version 1.0
 */
@Entity
@Table(name = "Client_Travel_Card")
public class ClientTravelCard implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "client_FK")
	private ClientAccount clientAccount;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "hotel_FK")
	private HotelReservation hotelReservation;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "plane_FK")
	private PlaneTicket planeTicket;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinTable(name = "Accompanying_Person", joinColumns = @JoinColumn(name = "travel_card_id"), inverseJoinColumns = @JoinColumn(name = "person_id"))
	private List<Person> accompanyingPersons;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "travel_FK")
	private TravelOffer travelOffer;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "client_review_FK")
	private ClientReview clientReview;

	public ClientTravelCard() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the clientAccount
	 */
	public ClientAccount getClientAccount() {
		return clientAccount;
	}

	/**
	 * @param clientAccount
	 *            the clientAccount to set
	 */
	public void setClientAccount(ClientAccount clientAccount) {
		this.clientAccount = clientAccount;
	}

	/**
	 * @return the hotelReservation
	 */
	public HotelReservation getHotelReservation() {
		return hotelReservation;
	}

	/**
	 * @param hotelReservation
	 *            the hotelReservation to set
	 */
	public void setHotelReservation(HotelReservation hotelReservation) {
		this.hotelReservation = hotelReservation;
	}

	/**
	 * @return the planeTicket
	 */
	public PlaneTicket getPlaneTicket() {
		return planeTicket;
	}

	/**
	 * @param planeTicket
	 *            the planeTicket to set
	 */
	public void setPlaneTicket(PlaneTicket planeTicket) {
		this.planeTicket = planeTicket;
	}

	/**
	 * @return the accompanyingPersons
	 */
	public List<Person> getAccompanyingPersons() {
		return accompanyingPersons;
	}

	/**
	 * @param accompanyingPersons
	 *            the accompanyingPersons to set
	 */
	public void setAccompanyingPersons(List<Person> accompanyingPersons) {
		this.accompanyingPersons = accompanyingPersons;
	}

	/**
	 * @return the travelOffer
	 */
	public TravelOffer getTravelOffer() {
		return travelOffer;
	}

	/**
	 * @param travelOffer
	 *            the travelOffer to set
	 */
	public void setTravelOffer(TravelOffer travelOffer) {
		this.travelOffer = travelOffer;
	}

	/**
	 * @return the clientReview
	 */
	public ClientReview getClientReview() {
		return clientReview;
	}

	/**
	 * @param clientReview
	 *            the clientReview to set
	 */
	public void setClientReview(ClientReview clientReview) {
		this.clientReview = clientReview;
	}

	public Integer travelCost() {
		return this.travelOffer.getOfficeCommission() + this.hotelReservation.getPrice()
				+ this.planeTicket.getTicketPrice() * this.hotelReservation.getPersonInRoom();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ClientTravelCard [id=" + id + ", clientAccount=" + clientAccount + ", hotelReservation="
				+ hotelReservation + ", planeTicket=" + planeTicket + ", accompanyingPersons=" + accompanyingPersons
				+ ", travelOffer=" + travelOffer + ", clientReview=" + clientReview + "]";
	}

}
