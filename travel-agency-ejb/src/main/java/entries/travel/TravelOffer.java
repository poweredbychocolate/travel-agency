package entries.travel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import entries.Address;
import entries.InternetMedia;
import entries.client.ClientReview;
import entries.hotels.HotelReservation;
import entries.planes.PlaneTicket;

/**
 * Entity implementation class for Entity: TravelOffer
 * 
 * @author Dawid
 * @since 11.05.2018
 * @version 1.0
 *
 */
@Entity
@Table(name = "Travel_Offer")
public class TravelOffer implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String TYPE_CREATE = "Create";
	public static final String TYPE_PREPARED_HOTELS = "Prepared hotels";
	public static final String TYPE_PREPARED_TRANSPORT = "Prepared transport";
	public static final String TYPE_READY = "Ready";
	public static final String TYPE_ACTIVE = "Active";
	public static final String TYPE_ARCHIVE = "Archive";

	public static final String SORT_TITLE_ASC = "Title:ASC";
	public static final String SORT_TITLE_DESC = "Title:DESC";
	public static final String SORT_PRICE_ASC = "Price:ASC";
	public static final String SORT_PRICE_DESC = "Price:DESC";
	public static final String SORT_DATE_ASC = "Date:ASC";
	public static final String SORT_DATE_DESC = "Date:DESC";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private String type;

	@Column(nullable = false)
	private String title;
	@Column(nullable = false)
	private Integer officeCommission = 0;
	/**
	 * price consist minimum hotel price and minimum plane tickets price, needed for
	 * lazy fetch hotels and plane
	 */
	@Column(nullable = false)
	private Integer minPrice = 0;
	@Lob
	@Column(nullable = false)
	private String description;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date startDate = new Date();
	/**
	 * Allow display newest offer
	 */
	@Column(nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date activationDate = new Date();

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "address_FK")
	private Address address;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinTable(name = "Travel_Hotel", joinColumns = @JoinColumn(name = "travel_id"), inverseJoinColumns = @JoinColumn(name = "hotel_id"))
	private Set<HotelReservation> hotels;
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinTable(name = "Travel_Plane", joinColumns = @JoinColumn(name = "travel_id"), inverseJoinColumns = @JoinColumn(name = "plane_id"))
	private Set<PlaneTicket> planes;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "internet_media_FK")
	private InternetMedia image;

	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
	@JoinColumn(name = "review_travel_FK")
	private Set<ClientReview> clientReviews;

	public TravelOffer() {
		super();
		this.description = "";
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the officeCommission
	 */
	public Integer getOfficeCommission() {
		return officeCommission;
	}

	/**
	 * @param officeCommission
	 *            the officeCommission to set
	 */
	public void setOfficeCommission(Integer officeCommission) {
		this.officeCommission = officeCommission;
	}

	/**
	 * @return the minPrice
	 */
	public Integer getMinPrice() {
		return minPrice;
	}

	/**
	 * @param minPrice
	 *            the minPrice to set
	 */
	public void setMinPrice(Integer minPrice) {
		this.minPrice = minPrice;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the activationDate
	 */
	public Date getActivationDate() {
		return activationDate;
	}

	/**
	 * @param activationDate
	 *            the activationDate to set
	 */
	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @return the hotels
	 */
	public Set<HotelReservation> getHotels() {
		return hotels;
	}

	/**
	 * @param hotels
	 *            the hotels to set
	 */
	public void setHotels(Set<HotelReservation> hotels) {
		this.hotels = hotels;
	}

	/**
	 * @return the planes
	 */
	public Set<PlaneTicket> getPlanes() {
		return planes;
	}

	/**
	 * @param planes
	 *            the planes to set
	 */
	public void setPlanes(List<PlaneTicket> planes) {
		this.planes = new LinkedHashSet<>(planes);
	}

	/**
	 * @return the hotels
	 */
	public List<HotelReservation> getHotelsList() {
		return new ArrayList<HotelReservation>(hotels);
	}

	/**
	 * @param hotels
	 *            the hotels to set
	 */
	public void setHotels(List<HotelReservation> hotels) {
		this.hotels = new LinkedHashSet<>(hotels);
	}

	/**
	 * @return the planes
	 */
	public List<PlaneTicket> getPlanesList() {
		return new ArrayList<PlaneTicket>(planes);
	}

	/**
	 * @param planes
	 *            the planes to set
	 */
	public void setPlanes(Set<PlaneTicket> planes) {
		this.planes = planes;
	}

	/**
	 * @return the image
	 */
	public InternetMedia getImage() {
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(InternetMedia image) {
		this.image = image;
	}

	/**
	 * @return the clientReviews
	 */
	public Set<ClientReview> getClientReviews() {
		return clientReviews;
	}

	/**
	 * @param clientReviews
	 *            the clientReviews to set
	 */
	public void setClientReviews(Set<ClientReview> clientReviews) {
		this.clientReviews = clientReviews;
	}

	/**
	 * @return the clientReviews
	 */
	public List<ClientReview> getClientReviewsList() {
		return new ArrayList<>(clientReviews);
	}

	/**
	 * @param clientReviews
	 *            the clientReviews to set
	 */
	public void setClientReviews(List<ClientReview> clientReviews) {
		this.clientReviews = new LinkedHashSet<>(clientReviews);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TravelOffer [id=" + id + ", type=" + type + ", title=" + title + ", officeCommission="
				+ officeCommission + ", minPrice=" + minPrice + ", description=" + description + ", startDate="
				+ startDate + ", activationDate=" + activationDate + ", address=" + address + "]";
	}

}