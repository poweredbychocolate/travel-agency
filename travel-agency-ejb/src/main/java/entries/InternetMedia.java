package entries;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: InternetMedia
 * 
 * @author Dawid
 * @since 19.04.2018
 * @version 1.0
 */
@Entity
@Table(name = "Internet_Media")
public class InternetMedia implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String CATEGORY_TRAVELS = "Travel Offer";
	public static final String TYPE_PNG = "image/png";
	public static final String TYPE_JPEG = "image/jpeg";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = true)
	private String name;
	@Column(nullable = true)
	private String category;
	@Lob
	@Column(nullable = false)
	private byte[] mediaBytes;
	@Column(nullable = false)
	private String type;

	public InternetMedia() {
		super();
	}

	/**
	 * @param id
	 *            media id
	 * @param mediaBytes
	 *            media as byte object
	 */
	public InternetMedia(Integer id, byte[] mediaBytes) {
		super();
		this.id = id;
		this.mediaBytes = mediaBytes;
	}

	/**
	 * @param id
	 *            media id
	 * @param mediaBytes
	 *            media as byte object
	 * @param type
	 *            media type
	 */
	public InternetMedia(Integer id, byte[] mediaBytes, String type) {
		super();
		this.id = id;
		this.mediaBytes = mediaBytes;
		this.type = type;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the mediaBytes
	 */
	public byte[] getMediaBytes() {
		return mediaBytes;
	}

	/**
	 * @param mediaBytes
	 *            the mediaBytes to set
	 */
	public void setMediaBytes(byte[] mediaBytes) {
		this.mediaBytes = mediaBytes;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InternetMedia [id=" + id + ", name=" + name + ", category=" + category + ", mediaBytes size="
				+ mediaBytes.length + ", type=" + type + "]";
	}

}
