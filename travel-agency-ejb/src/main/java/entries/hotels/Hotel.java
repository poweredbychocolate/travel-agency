package entries.hotels;

import java.io.Serializable;
import javax.persistence.*;

import entries.Address;

/**
 * Entity implementation class for Entity: Hotel
 * 
 * @author Dawid
 * @since 02.05.2018
 * @version 1.0
 *
 */
@Entity
public class Hotel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
	private String name;
	@Column(nullable = true)
	private String pageLink;
	@Lob
	@Column(nullable = false)
	private String description;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "hotel_address_FK")
	private Address hotelAddress;

	public Hotel() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the pageLink
	 */
	public String getPageLink() {
		return pageLink;
	}

	/**
	 * @param pageLink
	 *            the pageLink to set
	 */
	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the hotelAddress
	 */
	public Address getHotelAddress() {
		return hotelAddress;
	}

	/**
	 * @param hotelAddress
	 *            the hotelAddress to set
	 */
	public void setHotelAddress(Address hotelAddress) {
		this.hotelAddress = hotelAddress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Hotel [id=" + id + ", name=" + name + ", pageLink=" + pageLink + ", description=" + description
				+ ", hotelAddress=" + hotelAddress + "]";
	}

}
