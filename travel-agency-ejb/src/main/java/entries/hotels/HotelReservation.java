package entries.hotels;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: HotelReservation
 * 
 * @author Dawid
 * @since 12.06.2018
 * @version 1.0
 */
@Entity
@Table(name = "Hotel_Reservation")

public class HotelReservation implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String TYPE_EXTERNAL = "External Offer";
	public static final String TYPE_TRAVEL = "Travel Offer";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private String type = TYPE_EXTERNAL;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "hotel_FK")
	private Hotel hotel;

	@Column(nullable = false)
	private Integer personInRoom = 0;
	@Column(nullable = false)
	private Integer roomCount = 0;
	@Column(nullable = false)
	private Integer price = 0;
	@Column(nullable = false)
	private Integer soldRoomCount = 0;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date availableFrom;
	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date availableTo;

	public HotelReservation() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the hotel
	 */
	public Hotel getHotel() {
		return hotel;
	}

	/**
	 * @param hotel
	 *            the hotel to set
	 */
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	/**
	 * @return the personInRoom
	 */
	public Integer getPersonInRoom() {
		return personInRoom;
	}

	/**
	 * @param personInRoom
	 *            the personInRoom to set
	 */
	public void setPersonInRoom(Integer personInRoom) {
		this.personInRoom = personInRoom;
	}

	/**
	 * @return the roomCount
	 */
	public Integer getRoomCount() {
		return roomCount;
	}

	/**
	 * @param roomCount
	 *            the roomCount to set
	 */
	public void setRoomCount(Integer roomCount) {
		this.roomCount = roomCount;
	}

	/**
	 * @return the price
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(Integer price) {
		this.price = price;
	}

	/**
	 * @return the soldRoomCount
	 */
	public Integer getSoldRoomCount() {
		return soldRoomCount;
	}

	/**
	 * @param soldRoomCount
	 *            the soldRoomCount to set
	 */
	public void setSoldRoomCount(Integer soldRoomCount) {
		this.soldRoomCount = soldRoomCount;
	}

	/**
	 * @return the availableFrom
	 */
	public Date getAvailableFrom() {
		return availableFrom;
	}

	/**
	 * @param availableFrom
	 *            the availableFrom to set
	 */
	public void setAvailableFrom(Date availableFrom) {
		this.availableFrom = availableFrom;
	}

	/**
	 * @return the availableTo
	 */
	public Date getAvailableTo() {
		return availableTo;
	}

	/**
	 * @param availableTo
	 *            the availableTo to set
	 */
	public void setAvailableTo(Date availableTo) {
		this.availableTo = availableTo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HotelReservation [id=" + id + ", type=" + type + ", hotel=" + hotel + ", personInRoom=" + personInRoom
				+ ", roomCount=" + roomCount + ", price=" + price + ", soldRoomCount=" + soldRoomCount
				+ ", availableFrom=" + availableFrom + ", availableTo=" + availableTo + "]";
	}

}
