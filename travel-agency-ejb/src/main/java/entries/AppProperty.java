package entries;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: AppProperty
 * 
 * @author Dawid
 * @version 1.0
 * @since 23.06.2018
 *
 */
@Entity
@Table(name = "App_Property")
public class AppProperty implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	// @Column(length = 150)
	private String magicKey;
	@Column(nullable = false)
	private String magicValue;

	public AppProperty() {
		super();
	}

	/**
	 * @param magicKey
	 *            property key
	 * @param magicValue
	 *            property value
	 */
	public AppProperty(String magicKey, String magicValue) {
		this.magicKey = magicKey;
		this.magicValue = magicValue;
	}

	/**
	 * @return the magicKey
	 */
	public String getMagicKey() {
		return magicKey;
	}

	/**
	 * @param magicKey
	 *            the magicKey to set
	 */
	public void setMagicKey(String magicKey) {
		this.magicKey = magicKey;
	}

	/**
	 * @return the magicValue
	 */
	public String getMagicValue() {
		return magicValue;
	}

	/**
	 * @param magicValue
	 *            the magicValue to set
	 */
	public void setMagicValue(String magicValue) {
		this.magicValue = magicValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AppProperty [magicKey=" + magicKey + ", magicValue=" + magicValue + "]";
	}

}