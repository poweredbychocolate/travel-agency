package entries;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import entries.client.ClientAccount;
import entries.travel.TravelOffer;

/**
 * Entity implementation class for Entity: MailByCity
 * 
 * @author Dawid
 * @version 1.0
 * @since 24.06.2018
 *
 */
@Entity
@Table(name = "Custom_Newsletter")

public class CustomNewsletter implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "newsletter_client", nullable = false)
	private ClientAccount clientAccount;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "newsletter_travel", joinColumns = @JoinColumn(name = "newsletter"), inverseJoinColumns = @JoinColumn(name = "travel"))
	private List<TravelOffer> travelOffers;

	public CustomNewsletter() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the clientAccount
	 */
	public ClientAccount getClientAccount() {
		return clientAccount;
	}

	/**
	 * @param clientAccount
	 *            the clientAccount to set
	 */
	public void setClientAccount(ClientAccount clientAccount) {
		this.clientAccount = clientAccount;
	}

	/**
	 * @return the travelOffers
	 */
	public List<TravelOffer> getTravelOffers() {
		return travelOffers;
	}

	/**
	 * @param travelOffers
	 *            the travelOffers to set
	 */
	public void setTravelOffers(List<TravelOffer> travelOffers) {
		this.travelOffers = travelOffers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomNewsletter [id=" + id + ", clientAccount=" + clientAccount + ", travelOffers=" + travelOffers
				+ "]";
	}

}
