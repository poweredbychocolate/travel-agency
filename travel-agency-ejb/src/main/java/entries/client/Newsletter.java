package entries.client;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Newsletter
 * 
 * @author Dawid
 * @since 07.06.2018
 * @version 1.0
 */
@Entity

public class Newsletter implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private boolean newOffer = false;
	@Column(nullable = true)
	private String travelCountry;
	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "newsletter_Client_FK")
	private ClientAccount clientAccount;

	public Newsletter() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the newOffer
	 */
	public boolean getNewOffer() {
		return newOffer;
	}

	/**
	 * @param newOffer
	 *            the newOffer to set
	 */
	public void setNewOffer(boolean newOffer) {
		this.newOffer = newOffer;
	}

	/**
	 * @return the travelCountry
	 */
	public String getTravelCountry() {
		return travelCountry;
	}

	/**
	 * @param travelCountry
	 *            the travelCountry to set
	 */
	public void setTravelCountry(String travelCountry) {
		this.travelCountry = travelCountry;
	}

	/**
	 * @return the clientAccount
	 */
	public ClientAccount getClientAccount() {
		return clientAccount;
	}

	/**
	 * @param clientAccount
	 *            the clientAccount to set
	 */
	public void setClientAccount(ClientAccount clientAccount) {
		this.clientAccount = clientAccount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Newsletter [id=" + id + ", newOffer=" + newOffer + ", travelCountry=" + travelCountry
				+ ", clientAccount=" + clientAccount.getId() + "]";
	}

}