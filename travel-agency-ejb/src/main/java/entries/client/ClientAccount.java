package entries.client;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: ClientAccount
 *
 */
@Entity
@Table(name = "Client_Account")
public class ClientAccount implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String TYPE_ACTIVE = "Active";
	public static final String TYPE_CREATED = "Created";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
	private String type = TYPE_CREATED;
	@Column(nullable = false)

	@Temporal(TemporalType.DATE)
	private Date registerDate = new Date();

	@Column(nullable = false, unique = true)
	private String mail;
	@Column(nullable = false)
	private String password;

	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	@JoinColumn(name = "person_FK")
	private Person clientData;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "clientAccount", cascade = { CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	private Newsletter newsletter;

	public ClientAccount() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the registerDate
	 */
	public Date getRegisterDate() {
		return registerDate;
	}

	/**
	 * @param registerDate
	 *            the registerDate to set
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail
	 *            the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the clientData
	 */
	public Person getClientData() {
		return clientData;
	}

	/**
	 * @param clientData
	 *            the clientData to set
	 */
	public void setClientData(Person clientData) {
		this.clientData = clientData;
	}

	/**
	 * @return the newsletter
	 */
	public Newsletter getNewsletter() {
		return newsletter;
	}

	/**
	 * @param newsletter
	 *            the newsletter to set
	 */
	public void setNewsletter(Newsletter newsletter) {
		this.newsletter = newsletter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ClientAccount [id=" + id + ", type=" + type + ", registerDate=" + registerDate + ", mail=" + mail
				+ ", password=" + password + ", clientData=" + clientData + ", newsletter=" + newsletter.getId() + "]";
	}

}