package entries.client;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: ActivationToken
 * 
 * @author Dawid
 * @version 1.0
 * @since 05.06.2018
 */
@Entity
@Table(name = "Activation_Token")

public class ActivationToken implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private String token;
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date generatedDate = new Date();
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "token_client_FK", nullable = false)
	private ClientAccount clientAccount;

	public ActivationToken() {
		super();
	}

	/**
	 * @param id
	 *            id
	 * @param token
	 *            token string value
	 * @param generatedDate
	 *            string token generated date
	 * @param clientAccount
	 *            register {@link ClientAccount}
	 */
	public ActivationToken(Integer id, String token, Date generatedDate, ClientAccount clientAccount) {
		super();
		this.id = id;
		this.token = token;
		this.generatedDate = generatedDate;
		this.clientAccount = clientAccount;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token
	 *            the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the generatedDate
	 */
	public Date getGeneratedDate() {
		return generatedDate;
	}

	/**
	 * @param generatedDate
	 *            the generatedDate to set
	 */
	public void setGeneratedDate(Date generatedDate) {
		this.generatedDate = generatedDate;
	}

	/**
	 * @return the clientAccount
	 */
	public ClientAccount getClientAccount() {
		return clientAccount;
	}

	/**
	 * @param clientAccount
	 *            the clientAccount to set
	 */
	public void setClientAccount(ClientAccount clientAccount) {
		this.clientAccount = clientAccount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ActivationToken [id=" + id + ", token=" + token + ", generatedDate=" + generatedDate
				+ ", clientAccount=" + clientAccount + "]";
	}

}
