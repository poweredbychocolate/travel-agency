package entries.client;

import java.io.Serializable;
import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Entity implementation class for Entity: ClientReview
 * 
 * @author Dawid
 * @since 07.06.2018
 * @version 1.0
 */
@Entity
@Table(name = "Client_Review")
public class ClientReview implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private Integer rating=5;
	@Column(nullable = false)
	private String title;
	@Column(nullable = false)
	private String rewiew;

	public ClientReview() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the rating
	 */
	public Integer getRating() {
		return rating;
	}

	/**
	 * @param rating
	 *            the rating to set
	 */
	public void setRating(Integer rating) {
		this.rating = rating;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the rewiew
	 */
	public String getRewiew() {
		return rewiew;
	}

	/**
	 * @param rewiew
	 *            the rewiew to set
	 */
	public void setRewiew(String rewiew) {
		this.rewiew = rewiew;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ClientReview [id=" + id + ", rating=" + rating + ", title=" + title + ", rewiew=" + rewiew + "]";
	}

}
