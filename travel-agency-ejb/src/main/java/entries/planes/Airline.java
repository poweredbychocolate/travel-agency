package entries.planes;

import java.io.Serializable;
import javax.persistence.*;

import entries.Address;

/**
 * Entity implementation class for Entity: Plane
 * 
 * @author Dawid
 * @since 06.05.2018
 * @version 1.0
 */
@Entity

public class Airline implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
	private String name;

	@Column(nullable = true)
	private String pageLink;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "address_airlines_FK")
	private Address airlineOffice;

	public Airline() {
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the pageLink
	 */
	public String getPageLink() {
		return pageLink;
	}

	/**
	 * @param pageLink
	 *            the pageLink to set
	 */
	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}

	/**
	 * @return the airlineOffice
	 */
	public Address getAirlineOffice() {
		return airlineOffice;
	}

	/**
	 * @param airlineOffice
	 *            the airlineOffice to set
	 */
	public void setAirlineOffice(Address airlineOffice) {
		this.airlineOffice = airlineOffice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Airline [id=" + id + ", name=" + name + ", pageLink=" + pageLink + ", airlineOffice=" + airlineOffice
				+ "]";
	}

}
