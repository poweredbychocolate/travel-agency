package entries.planes;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: PlaneTicket
 * 
 * @author Dawid
 * @version 1.0
 * @since 12.06.2018
 */
@Entity
@Table(name = "Plane_Ticket")

public class PlaneTicket implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "airlines_FK")
	private Airline airlines;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "departure_airport_FK")
	private Airport departureAirport;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "target_airport_FK")
	private Airport targetAirport;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date departureDate;
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date returnDate;

	@Column(nullable = false)
	private Integer ticketCount = 1;
	@Column(nullable = false)
	private Integer ticketPrice = 0;
	@Column(nullable = false)
	private Integer soldTicket = 0;

	public PlaneTicket() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the airlines
	 */
	public Airline getAirlines() {
		return airlines;
	}

	/**
	 * @param airlines
	 *            the airlines to set
	 */
	public void setAirlines(Airline airlines) {
		this.airlines = airlines;
	}

	/**
	 * @return the departureAirport
	 */
	public Airport getDepartureAirport() {
		return departureAirport;
	}

	/**
	 * @param departureAirport
	 *            the departureAirport to set
	 */
	public void setDepartureAirport(Airport departureAirport) {
		this.departureAirport = departureAirport;
	}

	/**
	 * @return the returnAirport
	 */
	public Airport getTargetAirport() {
		return targetAirport;
	}

	/**
	 * @param targetAirport
	 *            the targetAirportt to set
	 */
	public void setTargetAirport(Airport targetAirport) {
		this.targetAirport = targetAirport;
	}

	/**
	 * @return the departureDate
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            the departureDate to set
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return the returnDate
	 */
	public Date getReturnDate() {
		return returnDate;
	}

	/**
	 * @param returnDate
	 *            the returnDate to set
	 */
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	/**
	 * @return the ticketCount
	 */
	public Integer getTicketCount() {
		return ticketCount;
	}

	/**
	 * @param ticketCount
	 *            the ticketCount to set
	 */
	public void setTicketCount(Integer ticketCount) {
		this.ticketCount = ticketCount;
	}

	/**
	 * @return the ticketPrice
	 */
	public Integer getTicketPrice() {
		return ticketPrice;
	}

	/**
	 * @param ticketPrice
	 *            the ticketPrice to set
	 */
	public void setTicketPrice(Integer ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	/**
	 * @return the soldTicket
	 */
	public Integer getSoldTicket() {
		return soldTicket;
	}

	/**
	 * @param soldTicket
	 *            the soldTicket to set
	 */
	public void setSoldTicket(Integer soldTicket) {
		this.soldTicket = soldTicket;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PlaneTicket [id=" + id + ", airlines=" + airlines + ", departureAirport=" + departureAirport
				+ ", targetAirport=" + targetAirport + ", departureDate=" + departureDate + ", returnDate=" + returnDate
				+ ", ticketCount=" + ticketCount + ", ticketPrice=" + ticketPrice + ", soldTicket=" + soldTicket + "]";
	}

}
