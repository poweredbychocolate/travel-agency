package classic;

import java.io.Serializable;

import com.itextpdf.io.source.ByteArrayOutputStream;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.UnitValue;

import entries.travel.ClientTravelCard;
import entries.travel.TravelOffer;

/**
 * 
 * @author Dawid
 * @version 1.0
 * @since 15.06.2018
 *
 */
public class PDF implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ClientTravelCard travelCard;

	/**
	 * @param travelCard {@link ClientTravelCard} used for generate pdf file 
	 */
	public PDF(ClientTravelCard travelCard) {
		super();
		this.travelCard = travelCard;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PDF [travelCard=" + travelCard + "]";
	}

	public byte[] bulid() {
		// TODO uzupelnic tresc
		System.err.println("[bulid pdf]");
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		Document document = new Document(new PdfDocument(new PdfWriter(output)), PageSize.A4);
		document.add(new Paragraph(travelCard.getTravelOffer().getTitle()).setBold().setFontSize(25.0f)
				.setBackgroundColor(ColorConstants.LIGHT_GRAY));

		document.add(new Paragraph(travelCard.getTravelOffer().getAddress().getCountry()).setBold());
		document.add(new Paragraph(travelCard.getTravelOffer().getAddress().getCity()));

		Table hotelTable = new Table(new UnitValue[] { new UnitValue(UnitValue.PERCENT, 20),
				new UnitValue(UnitValue.PERCENT, 20), new UnitValue(UnitValue.PERCENT, 20),
				new UnitValue(UnitValue.PERCENT, 20), new UnitValue(UnitValue.PERCENT, 20) });
		hotelTable.addCell(travelCard.getHotelReservation().getHotel().getName());
		hotelTable.addCell(travelCard.getHotelReservation().getHotel().getHotelAddress().getCity());
		hotelTable.addCell(travelCard.getHotelReservation().getHotel().getHotelAddress().getStreet());
		hotelTable.addCell(travelCard.getHotelReservation().getAvailableFrom().toString());
		hotelTable.addCell(travelCard.getHotelReservation().getAvailableTo().toString());
		document.add(hotelTable);
		document.close();
		System.err.println("[bulid pdf]");
		return output.toByteArray();
	}

}
