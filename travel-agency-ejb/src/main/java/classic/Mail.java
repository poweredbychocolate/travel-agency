/**
 * 
 */
package classic;

import java.io.Serializable;

/**
 * Mail bulider with default template
 * 
 * @author Dawid
 * @since 18.06.2018
 * @version 1.0
 *
 */
public abstract class Mail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String templateHead = "<?xml version='1.0' encoding='UTF-8' ?>"
			+ "<!DOCTYPE html><html lang=\"pl_PL\">" + "<head><title>Biuro podróży </title>"

			+ "<style> .master { margin: 0 auto; padding: 0;	width: 95%; }"

			+ ".travel_top { margin: 0 auto; text-align:center; padding: 0; min-height: 60px;"
			+ "background-color:#333333; font-size: 60px; color: #c26a03; }"

			+ ".travel_foot { background-color: #333333; text-align: center; color: white; width: auto;"
			+ " margin: 0; padding: 10px; min-height: 20px; }"

			+ ".main_content { min-height: 500px; background-color: #F2F2F2; } </style></head>";

	public static final String templateBodyTop = "<body><div id=\"master\" class=\"master\">"
			+ "<div id=\"top\" class=\"travel_top\"> Biuro podróży</div>"
			+ "<div id=\"main_content\" class=\"main_content\">";

	public static final String templateBodyButtom = "</div>"
			+ "<div id=\"foot\" class=\"travel_foot\"> Biuro podróży - wszystkie prawa zastrzeżone  ©</div>"
			+ "</div></body></html>";

	public abstract String recipients();

	public abstract String subject();

	public abstract String innerMailMessage();

	public String contentType() {
		return "text/html; charset=UTF-8";
	}

	public String mailContent() {
		StringBuilder mail = new StringBuilder();
		mail.append(templateHead + System.lineSeparator());
		mail.append(templateBodyTop + System.lineSeparator());
		mail.append(innerMailMessage());
		mail.append(templateBodyButtom + System.lineSeparator());

		return mail.toString();
	}
}
