/**
 * 
 */
package classic.mails;

import java.io.Serializable;

import classic.Mail;
import entries.travel.TravelOffer;

/**
 * 
 * @since 24.06.2016
 * @author Dawid
 * @version 1.0
 *
 */
public class NewsletterMailContent implements Serializable {
	private String subject;
	private String content;
	private StringBuilder stringBuilder;
	private String serverPath;
	private String mediaServetName;
	private String travelPageName;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NewsletterMailContent(String serverPath, String mediaServetName, String travelPageName) {
		this.serverPath = serverPath;
		this.mediaServetName = mediaServetName;
		this.travelPageName = travelPageName;
		stringBuilder = new StringBuilder(Mail.templateHead);
		stringBuilder.append(Mail.templateBodyTop);

	}

	public void bulid() {
		content = stringBuilder.toString();
	}

	public void appendTravel(TravelOffer travelOffer) {
		stringBuilder.append("<hr>");
		if (travelOffer.getImage() != null) {
			stringBuilder.append("<div style=\"width: 100%; height: 300px\"> <img src=\"" + serverPath + mediaServetName
					+ "=" + travelOffer.getImage().getId()
					+ " \" alt=\"Travel Image\" style=\"width: 100%; height: 300px; object-fit: cover; overflow: hidden;\"></div>");
		}

		stringBuilder.append("<h1> " + travelOffer.getTitle() + "</h1>");
		stringBuilder.append("<h3> " + travelOffer.getAddress().getCity() + "  " + travelOffer.getMinPrice() + "</h3>");
		stringBuilder.append("<h4> " + travelOffer.getDescription() + "</h4>");
		stringBuilder.append("<h4> <a href=\"" + serverPath + travelPageName + "=" + travelOffer.getId()
				+ "\">Przejdź do oferty</a></h4>");
		stringBuilder.append("<hr>");
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

}
