/**
 * 
 */
package classic.mails;

import classic.Mail;

/**
 * 
 * @since 24.06.2016
 * @author Dawid
 * @version 1.0
 *
 */
public class NewsletterMail extends Mail {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mailAddress;
	private NewsletterMailContent newsletterMailContent;

	/**
	 * @param mailAddress recipient address
	 * @param newsletterMailContent mailContent
	 */
	public NewsletterMail(String mailAddress, NewsletterMailContent newsletterMailContent) {
		this.mailAddress = mailAddress;
		this.newsletterMailContent = newsletterMailContent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see classic.Mail#recipients()
	 */
	@Override
	public String recipients() {
		// TODO Auto-generated method stub
		return mailAddress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see classic.Mail#subject()
	 */
	@Override
	public String subject() {
		// TODO Auto-generated method stub
		return newsletterMailContent.getSubject();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see classic.Mail#innerMailMessage()
	 */
	@Override
	public String innerMailMessage() {
		return null;
	}

	@Override
	public String mailContent() {
		return newsletterMailContent.getContent();
	}

}
