/**
 * 
 */
package classic.mails;

import java.io.Serializable;

import classic.Mail;
import entries.adminstration.AdministrationUser;

/**
 * 
 * @author Dawid
 * @version 1.0
 *
 */
public class AdminstrationUserMail extends Mail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NEW_ACCOUNT = "New Account";
	public static final String PASSWORD_CHANGE = "Password Change";
	private AdministrationUser administrationUser;
	private String type;

	/**
	 * @param administrationUser
	 *            {@link AdministrationUser} account
	 * @param type
	 *            generated mail content type
	 */
	public AdminstrationUserMail(AdministrationUser administrationUser, String type) {
		this.administrationUser = administrationUser;
		this.type = type;
	}

	@Override
	public String recipients() {
		return administrationUser.getMail();
	}

	@Override
	public String subject() {
		if (type.equals(NEW_ACCOUNT))
			return "Nowe konto użytkownika administracji";
		if (type.equals(PASSWORD_CHANGE))
			return "Zmiana hasła użytkownika administracji";
		return null;
	}

	@Override
	public String innerMailMessage() {
		if (type.equals(NEW_ACCOUNT))
			return newAccountMessage();
		if (type.equals(PASSWORD_CHANGE))
			return passChangeMessage();
		return null;
	}

	private String newAccountMessage() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("<h3> Nowe konto użytkownika administracji w systemie Biuro Podróży </h3>");
		stringBuilder.append("<h3> Login : " + administrationUser.getMail() + " </h3>");
		stringBuilder.append("<h3> Hasło : " + administrationUser.getHashPassword() + " </h3>");

		return stringBuilder.toString();
	}

	private String passChangeMessage() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(
				"<h3> Nowe hasło użytkownika( " + administrationUser.getMail() + " ) w systemie Biuro Podróży </h3>");
		stringBuilder.append("<h3> Hasło : " + administrationUser.getHashPassword() + " </h3>");

		return stringBuilder.toString();
	}

}
