package classic.mails;

import java.io.Serializable;

import classic.Mail;
import entries.client.ActivationToken;
import entries.client.ClientAccount;

/**
 * 
 * @author Dawid
 * @version 1.0
 * @since 15.06.2018
 *
 */
public class RegistrationMail extends Mail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ActivationToken activationToken;
	private ClientAccount clientAccount;
	private String servletURL;

	/**
	 * 
	 * @param activationToken
	 *            {@link ActivationToken} instance
	 * @param servletURL
	 *            servletURL without parameters
	 */
	public RegistrationMail(ActivationToken activationToken, String servletURL) {
		super();
		this.servletURL = servletURL;
		this.activationToken = activationToken;
		this.clientAccount = this.activationToken.getClientAccount();
	}

	public String recipients() {
		return clientAccount.getMail();
	}

	public String subject() {
		return "Rejestracja konta " + clientAccount.getMail();
	}

	public String innerMailMessage() {
		StringBuilder mail = new StringBuilder();
		mail.append("<h3> Ten mail (" + clientAccount.getMail()
				+ ") został użyty do rejestracji konta użytkownika. Kliknij poniższy link, aby aktywować swoje konto. </h3>");
		mail.append("<p><a href=\"" + servletURL + "?user=" + activationToken.getId() + "&key="
				+ activationToken.getToken() + "\"> ");
		mail.append(
				servletURL + "?user=" + activationToken.getId() + "&key=" + activationToken.getToken() + "</a></p>");

		mail.append(
				"<h5>Link pozostanie aktywny przez 24 godziny, jeśli to nie ty zakładałeś konto zignoruj tę wiadomość. </h5>");

		return mail.toString();
	}

}
