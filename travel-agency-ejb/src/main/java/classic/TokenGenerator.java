/**
 * 
 */
package classic;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.UUID;

/**
 * Alphanumeric token generator
 * 
 * @author Dawid
 * @version 1.0
 * @since 18.06.2018
 *
 */
public class TokenGenerator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String PATTERN = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private SecureRandom secureRandom;

	public TokenGenerator() {
		this.secureRandom = new SecureRandom();
	}

	public String simpleToken() {
		String token = UUID.randomUUID().toString();
		token.replace("-", "");
		return token;
	}

	public String randomToken(int length) {
		StringBuilder stringBuilder = new StringBuilder(length);
		for (int i = 0; i < length; i++) {
			stringBuilder.append(PATTERN.charAt(secureRandom.nextInt(PATTERN.length())));
		}
		return stringBuilder.toString();
	}

	public String hashString(String toHash) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.update(toHash.getBytes());
			byte[] hash = messageDigest.digest();
			return new String(hash, StandardCharsets.UTF_8);

		} catch (NoSuchAlgorithmException e) {
			return null;
		}

	}

}
