package enterpriseBean.dataProcessing;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import classic.Mail;
import enterpriseBean.dataProcessing.local.MailBeanLocal;
import enterpriseBean.dataProcessing.remote.MailBeanRemote;
import entries.AppProperty;
import entries.CustomNewsletter;
import entries.CustomNewsletter_;

import entries.client.ClientAccount;
import entries.client.ClientAccount_;
import entries.client.Newsletter;
import entries.client.Newsletter_;
import entries.travel.TravelOffer;
import entries.travel.TravelOffer_;

/**
 * Session Bean implementation class MailBean
 * 
 * @author Dawid
 * @version 1.0
 * @since 15.06.2018
 */
@Stateless

public class MailBean implements MailBeanRemote, MailBeanLocal {
	@Resource(name = "java:/TravelAgencyGMail")
	private Session session;
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public MailBean() {
	}

	@Override
	@Asynchronous
	public void sendMail(Mail mail) {
		try {
			System.err.println("[MailBean] Sending Mail");
			MimeMessage msg = new MimeMessage(session);
			msg.setHeader("Content-Type", "text/plain; charset=UTF-8");
			msg.setRecipients(RecipientType.TO, mail.recipients());
			msg.setSubject(mail.subject(), "utf-8");
			msg.setSentDate(new Date());
			msg.setContent(mail.mailContent(), mail.contentType());
			Transport.send(msg);
			System.err.println("[MailBean] Send Mail");
		} catch (Exception e) {
			System.err.println("[MailBean] Mail cannot be send !!");
		}

	}

	@Override
	public AppProperty findAppProperty(String key) {
		return entityManager.find(AppProperty.class, key);
	}

	@Override
	public AppProperty saveAppProperty(AppProperty appProperty) {
		return entityManager.merge(appProperty);

	}

	@Override
	public List<TravelOffer> getOfferUntilDate(Date date) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TravelOffer> query = builder.createQuery(TravelOffer.class);
		Root<TravelOffer> travel = query.from(TravelOffer.class);
		query.select(travel);
		query.where(builder.and(builder.greaterThan(travel.get(TravelOffer_.activationDate), date),
				builder.equal(travel.get(TravelOffer_.type), TravelOffer.TYPE_ACTIVE)));
		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public List<String> activeNewsletterMails() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<String> query = builder.createQuery(String.class);
		Root<Newsletter> newletter = query.from(Newsletter.class);
		query.select(newletter.get(Newsletter_.clientAccount).get(ClientAccount_.mail));
		query.where(builder.equal(newletter.get(Newsletter_.newOffer), true));
		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public List<Newsletter> findNewsletterByCountry(String country) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Newsletter> query = builder.createQuery(Newsletter.class);
		Root<Newsletter> newletter = query.from(Newsletter.class);
		query.select(newletter);
		query.where(builder.equal(newletter.get(Newsletter_.travelCountry), country));
		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public CustomNewsletter findCustomNewsletter(ClientAccount clientAccount) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<CustomNewsletter> query = builder.createQuery(CustomNewsletter.class);
		Root<CustomNewsletter> mailRoot = query.from(CustomNewsletter.class);
		query.select(mailRoot);
		query.where(builder.equal(mailRoot.get(CustomNewsletter_.clientAccount), clientAccount));
		CustomNewsletter mailByCity = null;
		try {
			mailByCity = entityManager.createQuery(query).getSingleResult();
			System.err.println("findMailByCity");
		} catch (NoResultException e) {
			mailByCity = new CustomNewsletter();
			mailByCity.setClientAccount(clientAccount);
			mailByCity.setTravelOffers(new ArrayList<>());
			System.err.println("findCustomNewsletter error");
		}
		return mailByCity;

	}

	@Override
	public CustomNewsletter saveCustomNewsletter(CustomNewsletter customNewsletter) {
		// entityManager.persist(customNewsletter);
		CustomNewsletter cNewsletter = entityManager.merge(customNewsletter);
		return cNewsletter;
	}

	@Override
	public List<CustomNewsletter> findCustomNewsletter() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<CustomNewsletter> query = builder.createQuery(CustomNewsletter.class);
		Root<CustomNewsletter> newletter = query.from(CustomNewsletter.class);
		query.select(newletter);
		query.where(builder.greaterThan(builder.size(newletter.get(CustomNewsletter_.travelOffers)), 0));
		return entityManager.createQuery(query).getResultList();
	}

}
