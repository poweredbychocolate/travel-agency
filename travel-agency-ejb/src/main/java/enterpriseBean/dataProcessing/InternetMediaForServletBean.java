package enterpriseBean.dataProcessing;

import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import enterpriseBean.dataProcessing.local.InternetMediaForServletBeanLocal;
import entries.InternetMedia;
import entries.InternetMedia_;

/**
 * Session Bean implementation class InternetMediaForServletBean. This bean is
 * used for load {@link InternetMedia} entries
 * 
 * @author Dawid
 * @since 23.05.2018
 * @version 1.0
 */
@Singleton
public class InternetMediaForServletBean implements InternetMediaForServletBeanLocal {

	@PersistenceContext
	private EntityManager entityManager;
	private ConcurrentHashMap<Integer, InternetMedia> internetMediaCache;

	/**
	 * Default constructor.
	 */
	public InternetMediaForServletBean() {
		this.internetMediaCache = new ConcurrentHashMap<>();
	}

	@Override
	public InternetMedia find(Integer id) {
		if (internetMediaCache.containsKey(id)) {
			return internetMediaCache.get(id);
		} else {
			// load only field used by media servlet
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<InternetMedia> query = builder.createQuery(InternetMedia.class);
			Root<InternetMedia> internetMedia = query.from(InternetMedia.class);
			query.select(builder.construct(InternetMedia.class, internetMedia.get(InternetMedia_.id),
					internetMedia.get(InternetMedia_.mediaBytes), internetMedia.get(InternetMedia_.type)));
			query.where(builder.equal(internetMedia.get(InternetMedia_.id), id));
			InternetMedia media = entityManager.createQuery(query).getSingleResult();
			if (media != null) {
				internetMediaCache.put(media.getId(), media);
				return media;
			}
		}
		return null;
	}

	@Override
	public void clearMap() {
		this.internetMediaCache.clear();
	}

}
