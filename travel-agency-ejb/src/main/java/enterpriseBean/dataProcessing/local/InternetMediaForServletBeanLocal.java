package enterpriseBean.dataProcessing.local;

import javax.ejb.Local;

import entries.InternetMedia;

@Local
public interface InternetMediaForServletBeanLocal {

	public InternetMedia find(Integer id);

	public void clearMap();


}
