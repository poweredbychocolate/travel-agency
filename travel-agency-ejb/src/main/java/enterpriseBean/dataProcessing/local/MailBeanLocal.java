package enterpriseBean.dataProcessing.local;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import classic.Mail;
import entries.AppProperty;
import entries.CustomNewsletter;
import entries.client.ClientAccount;
import entries.client.Newsletter;
import entries.travel.TravelOffer;

@Local
public interface MailBeanLocal {
	public void sendMail(Mail mail);

	AppProperty findAppProperty(String key);

	AppProperty saveAppProperty(AppProperty appProperty);

	List<TravelOffer> getOfferUntilDate(Date date);

	List<String> activeNewsletterMails();

	List<Newsletter> findNewsletterByCountry(String country);

	CustomNewsletter findCustomNewsletter(ClientAccount clientAccount);

	CustomNewsletter saveCustomNewsletter(CustomNewsletter customNewsletter);

	List<CustomNewsletter> findCustomNewsletter();

}
