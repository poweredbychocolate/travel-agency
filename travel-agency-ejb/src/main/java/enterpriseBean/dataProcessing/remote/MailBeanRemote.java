package enterpriseBean.dataProcessing.remote;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import classic.Mail;
import entries.AppProperty;
import entries.travel.TravelOffer;

@Remote
public interface MailBeanRemote {
	public void sendMail(Mail mail);

	AppProperty findAppProperty(String key);

	AppProperty saveAppProperty(AppProperty appProperty);

	List<TravelOffer> getOfferUntilDate(Date date);

	List<String> activeNewsletterMails();

}
