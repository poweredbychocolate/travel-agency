package enterpriseBean.administration.remote;

import java.util.List;

import javax.ejb.Remote;

import entries.adminstration.AdministrationUser;

/**
 * 
 * @author Dawid
 * @since 22.04.2018
 * @version 1.0
 *
 */
@Remote
public interface AdministrationUsersBeanRemote {

	public AdministrationUser getUserIfExist(String mail, String hashPassword);

	public List<AdministrationUser> getUsers();

	public void saveAdministrationUser(AdministrationUser user);

	public void changeAdministrationUser(AdministrationUser user);

}
