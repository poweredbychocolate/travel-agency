package enterpriseBean.administration.remote;

import java.util.List;

import javax.ejb.Remote;

import entries.AppProperty;
import entries.hotels.HotelReservation;
import entries.planes.PlaneTicket;
import entries.travel.TravelOffer;

/**
 * 
 * @author Dawid
 * @version 1.0
 * @since 02.05.2018
 */
@Remote
public interface TravelFactoryBeanRemote {
	List<TravelOffer> getOfferByStatus(String type);

	TravelOffer saveOffer(TravelOffer travel);

	void removeOffer(TravelOffer travel);

	List<HotelReservation> getHotelsByCriteria(String searchHotel, String type);

	List<PlaneTicket> getPlaneByCriteria(String searchPlane);

	TravelOffer refreshOffer(TravelOffer travel);

	List<TravelOffer> getFullyOfferByStatus(String type);

	List<TravelOffer> getFullyOfferByCriteria(String type, String cityFilter);

	List<TravelOffer> getOfferInProgress();

	List<String> travelCitiesByCriteria(String... types);

	List<TravelOffer> getOfferInProgress(String cityFilter);

	

}
