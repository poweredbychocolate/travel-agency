package enterpriseBean.administration;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import enterpriseBean.administration.local.AdministrationUsersBeanLocal;
import enterpriseBean.administration.remote.AdministrationUsersBeanRemote;
import entries.adminstration.AdministrationUser;

/**
 * Session Bean implementation class UsersBean
 * 
 * @author Dawid
 * @since 22.04.2018
 * @version 1.0
 */
@Stateless
public class AdministrationUsersBean implements AdministrationUsersBeanRemote, AdministrationUsersBeanLocal {

	/**
	 * Default constructor.
	 */
	@PersistenceContext
	private EntityManager entityManager;

	public AdministrationUsersBean() {
	}

	@Override
	public void saveAdministrationUser(AdministrationUser user) {
		entityManager.persist(user);
	}

	@Override
	public void changeAdministrationUser(AdministrationUser user) {
		entityManager.merge(user);
	}

	/**
	 * Return {@link AdministrationUser} instance if find user with correspond
	 * parameters, otherwise return null
	 * 
	 * @param mail
	 *            administration mail
	 * @param hashPassword
	 *            administration password
	 * @return {@link AdministrationUser } or null
	 */

	@Override
	public AdministrationUser getUserIfExist(String mail, String hashPassword) {
		TypedQuery<AdministrationUser> query = entityManager.createQuery(
				"SELECT u FROM AdministrationUser u WHERE u.mail = :mail AND u.hashPassword = :pass AND u.disabled = :disabled",
				AdministrationUser.class);
		query.setParameter("mail", mail);
		query.setParameter("pass", hashPassword);
		query.setParameter("disabled", Boolean.FALSE);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

	}

	@Override
	public List<AdministrationUser> getUsers() {
		TypedQuery<AdministrationUser> query = entityManager.createQuery("SELECT u FROM AdministrationUser u ",
				AdministrationUser.class);

		return query.getResultList();

	}

}
