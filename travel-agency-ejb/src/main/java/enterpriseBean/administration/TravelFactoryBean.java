/**
 * This bean is for create travel offer by administration user. Allow user build and configure  offer  
 */
package enterpriseBean.administration;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import enterpriseBean.administration.local.TravelFactoryBeanLocal;
import enterpriseBean.administration.remote.TravelFactoryBeanRemote;
import entries.Address_;
import entries.hotels.HotelReservation;
import entries.hotels.HotelReservation_;
import entries.hotels.Hotel_;
import entries.planes.Airport_;
import entries.planes.PlaneTicket;
import entries.planes.PlaneTicket_;
import entries.travel.TravelOffer;
import entries.travel.TravelOffer_;

/**
 * Session Bean implementation class TravelFactoryBean
 * 
 * @author Dawid
 * @version 1.0
 * @since 02.05.2018
 */
@Stateless
public class TravelFactoryBean implements TravelFactoryBeanRemote, TravelFactoryBeanLocal {

	/**
	 * Default constructor.
	 */
	public TravelFactoryBean() {
	}

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<TravelOffer> getOfferByStatus(String type) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TravelOffer> query = builder.createQuery(TravelOffer.class);
		Root<TravelOffer> travel = query.from(TravelOffer.class);
		query.select(travel);
		query.where(builder.equal(travel.get(TravelOffer_.type), type));
		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public List<TravelOffer> getOfferInProgress() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TravelOffer> query = builder.createQuery(TravelOffer.class);
		Root<TravelOffer> travel = query.from(TravelOffer.class);
		query.where(builder.or(builder.equal(travel.get(TravelOffer_.type), TravelOffer.TYPE_CREATE),
				builder.equal(travel.get(TravelOffer_.type), TravelOffer.TYPE_PREPARED_HOTELS)));
		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public List<TravelOffer> getOfferInProgress(String cityFilter) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TravelOffer> query = builder.createQuery(TravelOffer.class);
		Root<TravelOffer> travel = query.from(TravelOffer.class);
		if (cityFilter == null) {
			query.where(builder.or(builder.equal(travel.get(TravelOffer_.type), TravelOffer.TYPE_CREATE),
					builder.equal(travel.get(TravelOffer_.type), TravelOffer.TYPE_PREPARED_HOTELS)));
		} else {
			query.where(
					builder.or(builder.equal(travel.get(TravelOffer_.type), TravelOffer.TYPE_CREATE),
							builder.equal(travel.get(TravelOffer_.type), TravelOffer.TYPE_PREPARED_HOTELS)),
					builder.and(builder.equal(travel.get(TravelOffer_.address).get(Address_.city), cityFilter)));
		}

		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public List<TravelOffer> getFullyOfferByStatus(String type) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TravelOffer> query = builder.createQuery(TravelOffer.class);
		Root<TravelOffer> travel = query.from(TravelOffer.class);
		travel.fetch(TravelOffer_.hotels, JoinType.LEFT);
		travel.fetch(TravelOffer_.planes, JoinType.LEFT);
		travel.fetch(TravelOffer_.clientReviews, JoinType.LEFT);
		query.select(travel).distinct(true);
		query.where(builder.equal(travel.get(TravelOffer_.type), type));
		List<TravelOffer> list = entityManager.createQuery(query).getResultList();
		return list;
	}

	@Override
	public TravelOffer saveOffer(TravelOffer travel) {
		return entityManager.merge(travel);
	}

	@Override
	public void removeOffer(TravelOffer travel) {
		entityManager.remove(entityManager.merge(travel));
	}

	@Override
	public List<HotelReservation> getHotelsByCriteria(String searchHotel, String type) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<HotelReservation> query = builder.createQuery(HotelReservation.class);
		Root<HotelReservation> hotel = query.from(HotelReservation.class);
		query.select(hotel);
		query.where(builder.and(builder.equal(hotel.get(HotelReservation_.type), type), builder
				.equal(hotel.get(HotelReservation_.hotel).get(Hotel_.hotelAddress).get(Address_.city), searchHotel)));
		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public List<PlaneTicket> getPlaneByCriteria(String searchPlane) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<PlaneTicket> query = builder.createQuery(PlaneTicket.class);
		Root<PlaneTicket> plane = query.from(PlaneTicket.class);
		query.select(plane);
		query.where(builder.equal(plane.get(PlaneTicket_.targetAirport).get(Airport_.address).get(Address_.city),
				searchPlane));
		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public TravelOffer refreshOffer(TravelOffer travel) {
		return entityManager.merge(travel);
	}

	@Override
	public List<TravelOffer> getFullyOfferByCriteria(String type, String cityFilter) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TravelOffer> query = builder.createQuery(TravelOffer.class);
		Root<TravelOffer> travel = query.from(TravelOffer.class);
		travel.fetch(TravelOffer_.hotels, JoinType.LEFT);
		travel.fetch(TravelOffer_.planes, JoinType.LEFT);
		query.select(travel).distinct(true);

		if (cityFilter == null) {
			query.where(builder.equal(travel.get(TravelOffer_.type), type));
		} else {
			query.where(builder.and(builder.equal(travel.get(TravelOffer_.type), type),
					builder.equal(travel.get(TravelOffer_.address).get(Address_.city), cityFilter)));
		}
		List<TravelOffer> list = entityManager.createQuery(query).getResultList();
		return list;
	}

	@Override
	public List<String> travelCitiesByCriteria(String... types) {
		StringBuilder q = new StringBuilder(" SELECT DISTINCT a.city FROM TravelOffer t LEFT JOIN t.address a ");
		// if types has first element add where clause with parameter
		if (types.length > 0) {
			q.append(" WHERE t.type= ?1 ");
		}
		// if types has more then one element generate OR clause with parameters start
		// with counting from two
		if (types.length > 1) {
			for (int i = 2; i <= types.length; i++) {
				q.append(" OR t.type= ?" + i);
			}
		}
		q.append(" ORDER BY a.city DESC ");
		TypedQuery<String> query = entityManager.createQuery(q.toString(), String.class);
		// if types has first element set it as first parameter
		if (types.length > 0) {
			query.setParameter(1, types[0]);
			// if types has more then one element set is as parameters
		}
		if (types.length > 1) {
			for (int i = 1; i < types.length; i++) {
				query.setParameter(i + 1, types[i]);
			}
		}
		return query.getResultList();
	}

}
