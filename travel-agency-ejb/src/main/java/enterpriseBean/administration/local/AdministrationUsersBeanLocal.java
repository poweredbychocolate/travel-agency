package enterpriseBean.administration.local;

import java.util.List;

import javax.ejb.Local;

import entries.adminstration.AdministrationUser;

/**
 * 
 * @author Dawid
 * @since 22.04.2018
 * @version 1.0
 *
 */
@Local
public interface AdministrationUsersBeanLocal {

	public AdministrationUser getUserIfExist(String mail, String hashPassword);

	public List<AdministrationUser> getUsers();

	public void saveAdministrationUser(AdministrationUser user);

	public void changeAdministrationUser(AdministrationUser user);

}
