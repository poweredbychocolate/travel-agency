package enterpriseBean.client;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import enterpriseBean.client.local.TravelOfferBeanLocal;
import enterpriseBean.client.remote.TravelOfferBeanRemote;
import entries.Address_;
import entries.client.ClientAccount;
import entries.client.Person;
import entries.hotels.Hotel;
import entries.travel.TravelOffer;
import entries.travel.TravelOffer_;
import entries.travel.ClientTravelCard;
import entries.travel.ClientTravelCard_;

/**
 * Session Bean implementation class TravelOfferBean
 * 
 * @author Dawid
 * @version 1.0
 * @since 26.04.2018
 * 
 */
@Stateless
public class TravelOfferBean implements TravelOfferBeanRemote, TravelOfferBeanLocal {

	/**
	 * Default constructor.
	 */
	@PersistenceContext
	private EntityManager entityManager;

	public TravelOfferBean() {
	}

	@Override
	public void saveActiveOffer(TravelOffer travelOffer) {
		entityManager.merge(travelOffer);
	}

	@Override
	public ClientTravelCard saveClientTravelCard(ClientTravelCard clientTravelCard) {
		return entityManager.merge(clientTravelCard);
	}

	@Override
	public void saveHotel(Hotel hotel) {
		entityManager.merge(hotel);
	}

	/*
	 * @Override public void savePlane(Plane plane) { entityManager.merge(plane); }
	 */

	@Override
	public List<TravelOffer> getAllActiveOffer() {
		// List<TravelOffer> list = null;
		// TypedQuery<TravelOffer> query = entityManager.createQuery("SELECT t FROM
		// TravelOffer t", TravelOffer.class);
		// list = query.getResultList();
		// return list;
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TravelOffer> query = builder.createQuery(TravelOffer.class);
		Root<TravelOffer> travel = query.from(TravelOffer.class);
		query.select(travel);
		query.where(builder.equal(travel.get(TravelOffer_.type), TravelOffer.TYPE_ACTIVE));
		query.orderBy(builder.asc(travel.get(TravelOffer_.startDate)));
		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public List<TravelOffer> getAllActiveOffer(String city, String sortOrder) {

		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TravelOffer> query = builder.createQuery(TravelOffer.class);
		Root<TravelOffer> travel = query.from(TravelOffer.class);
		query.select(travel);
		// select by city
		if (city == null) {
			query.where(builder.equal(travel.get(TravelOffer_.type), TravelOffer.TYPE_ACTIVE));
		} else {
			query.where(builder.and(builder.equal(travel.get(TravelOffer_.type), TravelOffer.TYPE_ACTIVE),
					builder.equal(travel.get(TravelOffer_.address).get(Address_.city), city)));
		}
		// Map sort order
		if (sortOrder == null) {
			query.orderBy(builder.asc(travel.get(TravelOffer_.title)));
		} else {

			if (sortOrder.equals(TravelOffer.SORT_TITLE_ASC)) {
				query.orderBy(builder.asc(travel.get(TravelOffer_.title)));
			} else if (sortOrder.equals(TravelOffer.SORT_TITLE_DESC)) {
				query.orderBy(builder.desc(travel.get(TravelOffer_.title)));

			} else if (sortOrder.equals(TravelOffer.SORT_PRICE_ASC)) {
				query.orderBy(builder.asc(travel.get(TravelOffer_.minPrice)));
			} else if (sortOrder.equals(TravelOffer.SORT_PRICE_DESC)) {
				query.orderBy(builder.desc(travel.get(TravelOffer_.minPrice)));

			} else if (sortOrder.equals(TravelOffer.SORT_DATE_ASC)) {
				query.orderBy(builder.asc(travel.get(TravelOffer_.startDate)));
			} else if (sortOrder.equals(TravelOffer.SORT_DATE_DESC)) {
				query.orderBy(builder.desc(travel.get(TravelOffer_.startDate)));

			} else {
				query.orderBy(builder.asc(travel.get(TravelOffer_.title)));
			}
		}
		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public List<TravelOffer> getNewestOffer(int count) {
		// String q = "SELECT t FROM TravelOffer t ORDER BY t.activationDate DESC";
		// TypedQuery<TravelOffer> query = entityManager.createQuery(q,
		// TravelOffer.class).setMaxResults(count);
		// return query.getResultList();
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TravelOffer> query = builder.createQuery(TravelOffer.class);
		Root<TravelOffer> travel = query.from(TravelOffer.class);
		query.select(travel);
		query.where(builder.equal(travel.get(TravelOffer_.type), TravelOffer.TYPE_ACTIVE));
		query.orderBy(builder.desc(travel.get(TravelOffer_.activationDate)));

		return entityManager.createQuery(query).setMaxResults(count).getResultList();

	}

	@Override
	public List<Hotel> loadHotels(Integer id) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Hotel> query = builder.createQuery(Hotel.class);
		Root<Hotel> hotel = query.from(Hotel.class);
		query.select(hotel);
		query.where(builder.equal(hotel.get("travelOffer"), id));
		query.orderBy(builder.asc(hotel.get("price")));
		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public List<ClientTravelCard> getUserOffer(ClientAccount clientAccount) {

		TypedQuery<ClientTravelCard> query = entityManager.createQuery(
				"SELECT c FROM ClientTravelCard c WHERE c.clientAccount =:id ORDER BY c.travelOffer.startDate",
				ClientTravelCard.class);
		query.setParameter("id", clientAccount);
		return query.getResultList();

		// return null;
	}

	@Override
	public TravelOffer findTravelOffer(int id) {
		return entityManager.find(TravelOffer.class, id);
	}

	@Override
	public TravelOffer findFullyTravelOffer(int id) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TravelOffer> query = builder.createQuery(TravelOffer.class);
		Root<TravelOffer> travel = query.from(TravelOffer.class);
		travel.fetch(TravelOffer_.hotels, JoinType.LEFT);
		travel.fetch(TravelOffer_.planes, JoinType.LEFT);
		travel.fetch(TravelOffer_.clientReviews, JoinType.LEFT);
		query.select(travel).distinct(true);
		query.where(builder.equal(travel.get(TravelOffer_.id), id));
		query.orderBy(builder.asc(travel.get(TravelOffer_.type)));

		TravelOffer offer = null;
		try {
			offer = entityManager.createQuery(query).getSingleResult();
		} catch (NoResultException e) {
			offer = null;
		}
		return offer;
	}

	@Override
	public ClientTravelCard findFullyClientTravelCard(int id) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ClientTravelCard> query = builder.createQuery(ClientTravelCard.class);
		Root<ClientTravelCard> card = query.from(ClientTravelCard.class);
		Fetch<ClientTravelCard, TravelOffer> travel = card.fetch(ClientTravelCard_.travelOffer, JoinType.LEFT);
		// travel.fetch(TravelOffer_.hotels, JoinType.LEFT);
		// travel.fetch(TravelOffer_.planes, JoinType.LEFT);
		travel.fetch(TravelOffer_.clientReviews, JoinType.LEFT);
		query.select(card).distinct(true);
		query.where(builder.equal(card.get(ClientTravelCard_.id), id));

		ClientTravelCard travelCard = null;
		try {
			travelCard = entityManager.createQuery(query).getSingleResult();
		} catch (NoResultException e) {
			travelCard = null;
		}
		return travelCard;
	}

	@Override
	public List<String> travelCities() {
		String q = "SELECT DISTINCT a.city FROM TravelOffer t LEFT JOIN t.address a WHERE t.type=:type ORDER BY a.city";
		TypedQuery<String> query = entityManager.createQuery(q, String.class);
		return query.setParameter("type", TravelOffer.TYPE_ACTIVE).getResultList();
	}

	@Override
	public List<Person> clientPersonsList(Integer id) {
		String q = "SELECT DISTINCT p FROM ClientTravelCard c LEFT JOIN c.accompanyingPersons p WHERE c.clientAccount.id=:id ORDER BY p.firstName";
		TypedQuery<Person> query = entityManager.createQuery(q, Person.class);
		return query.setParameter("id", id).getResultList();
	}

}