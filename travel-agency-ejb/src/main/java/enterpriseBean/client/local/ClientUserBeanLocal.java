package enterpriseBean.client.local;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import entries.client.ActivationToken;
import entries.client.ClientAccount;

@Local
public interface ClientUserBeanLocal {

	ClientAccount saveClientAccount(ClientAccount clientAccount);

	ClientAccount findUser(String mail, String pass);

	void saveActivationToken(ActivationToken activationToken);

	ActivationToken findToken(Integer id);

	void removeActivationToken(ActivationToken activationToken);

	void removeClientAccount(ClientAccount clientAccount);

	List<ActivationToken> outdatedActivationTokens(Date date);
}
