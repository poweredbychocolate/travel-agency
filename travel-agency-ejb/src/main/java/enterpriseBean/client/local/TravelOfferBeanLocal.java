package enterpriseBean.client.local;

import java.util.List;

import javax.ejb.Local;

import entries.client.ClientAccount;
import entries.client.Person;
import entries.hotels.Hotel;
import entries.travel.TravelOffer;
import entries.travel.ClientTravelCard;

/**
 * @author Dawid
 * @version 1.0
 * @since 26.04.2018
 *
 */
@Local
public interface TravelOfferBeanLocal {

	TravelOffer findTravelOffer(int id);

	TravelOffer findFullyTravelOffer(int id);

	List<TravelOffer> getAllActiveOffer();

	List<TravelOffer> getAllActiveOffer(String city,String sortOrder);

	List<TravelOffer> getNewestOffer(int count);

	 List<Hotel> loadHotels(Integer id);

	// public List<Plane> loadPlanes(Integer id);

	void saveHotel(Hotel hotel);

	// public void savePlane(Plane plane);

	ClientTravelCard saveClientTravelCard(ClientTravelCard clientTravelCard);

	 void saveActiveOffer(TravelOffer travelOffer);

	List<ClientTravelCard> getUserOffer(ClientAccount clientAccount);

	List<String> travelCities();

	List<Person> clientPersonsList(Integer id);

	ClientTravelCard findFullyClientTravelCard(int id);

}
