package enterpriseBean.client.remote;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import entries.client.ActivationToken;
import entries.client.ClientAccount;

@Remote
public interface ClientUserBeanRemote {

	ClientAccount saveClientAccount(ClientAccount clientAccount);

	ClientAccount findUser(String mail, String pass);

	void saveActivationToken(ActivationToken activationToken);

	ActivationToken findToken(Integer id);

	void removeActivationToken(ActivationToken activationToken);

	void removeClientAccount(ClientAccount clientAccount);
	
	List<ActivationToken> outdatedActivationTokens(Date date);
}
