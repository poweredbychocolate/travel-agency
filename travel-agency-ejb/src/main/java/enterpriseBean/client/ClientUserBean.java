package enterpriseBean.client;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import enterpriseBean.client.local.ClientUserBeanLocal;
import enterpriseBean.client.remote.ClientUserBeanRemote;
import entries.client.ActivationToken;
import entries.client.ActivationToken_;
import entries.client.ClientAccount;
import entries.client.ClientAccount_;
import entries.client.Newsletter;

/**
 * Session Bean implementation class ClientUserBean
 * 
 * @author Dawid
 * @version 1.0
 */
@Stateless
public class ClientUserBean implements ClientUserBeanRemote, ClientUserBeanLocal {

	/**
	 * Default constructor.
	 */
	@PersistenceContext
	private EntityManager entityManager;

	public ClientUserBean() {
	}

	@Override
	public ClientAccount saveClientAccount(ClientAccount clientAccount) {
		if (clientAccount.getNewsletter() == null) {

			Newsletter newsletter = new Newsletter();
			newsletter.setNewOffer(false);
			newsletter.setTravelCountry(null);

			newsletter.setClientAccount(clientAccount);
			clientAccount.setNewsletter(newsletter);

		}

		try {
			ClientAccount cAccount = entityManager.merge(clientAccount);
			return cAccount;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void saveActivationToken(ActivationToken activationToken) {
		entityManager.persist(activationToken);
	}

	@Override
	public ClientAccount findUser(String mail, String pass) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ClientAccount> query = builder.createQuery(ClientAccount.class);
		Root<ClientAccount> client = query.from(ClientAccount.class);
		query.where(builder.and(builder.equal(client.get(ClientAccount_.mail), mail),
				builder.equal(client.get(ClientAccount_.password), pass),
				builder.equal(client.get(ClientAccount_.type), ClientAccount.TYPE_ACTIVE)));
		query.select(client);
		try {
			return entityManager.createQuery(query).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public ActivationToken findToken(Integer id) {
		return entityManager.find(ActivationToken.class, id);
	}

	@Override
	public void removeActivationToken(ActivationToken activationToken) {
		entityManager.remove(entityManager.merge(activationToken));

	}

	@Override
	public void removeClientAccount(ClientAccount clientAccount) {
		entityManager.remove(entityManager.merge(clientAccount));
	}

	@Override
	public List<ActivationToken> outdatedActivationTokens(Date date) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ActivationToken> query = builder.createQuery(ActivationToken.class);
		Root<ActivationToken> token = query.from(ActivationToken.class);
		query.where(builder.lessThanOrEqualTo(token.get(ActivationToken_.generatedDate), date));
		query.select(token);
		TypedQuery<ActivationToken> typedQuery = entityManager.createQuery(query);
		return typedQuery.getResultList();
	}

}
