package enterpriseBean;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import classic.mails.NewsletterMail;
import classic.mails.NewsletterMailContent;
import enterpriseBean.administration.local.TravelFactoryBeanLocal;
import enterpriseBean.client.local.ClientUserBeanLocal;
import enterpriseBean.dataProcessing.InternetMediaForServletBean;
import enterpriseBean.dataProcessing.local.InternetMediaForServletBeanLocal;
import enterpriseBean.dataProcessing.local.MailBeanLocal;
import entries.AppProperty;
import entries.CustomNewsletter;
import entries.InternetMedia;
import entries.client.ActivationToken;
import entries.client.ClientAccount;
import entries.client.Newsletter;
import entries.hotels.HotelReservation;
import entries.planes.PlaneTicket;
import entries.travel.TravelOffer;

/**
 * Session Bean implementation class ScheduleBean. Check and execute
 * periodically task
 * 
 * @author Dawid
 * @since 10.05.2018
 * @version 1.0
 */
@Startup
@Singleton
@LocalBean
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class ScheduleBean {

	@EJB
	private TravelFactoryBeanLocal factoryBean;
	@EJB
	private InternetMediaForServletBeanLocal mediaBean;
	@EJB
	private ClientUserBeanLocal clientUserBean;
	@EJB
	private MailBeanLocal mailBean;

	/**
	 * Default constructor.
	 */
	public ScheduleBean() {

	}

	/**
	 * Periodically check {@link TravelOfferCard} and create offer for user if match
	 * criteria
	 */
	@Schedule(second = "*/30", minute = "*", hour = "*", persistent = false)
	private void travelSchedule() {
		System.err.println("[Travel Schedule] : " + new Date());
		activeOffers();
		System.err.println("[Travel Schedule] : " + new Date());
	}

	/**
	 * Periodically remove all {@link InternetMedia} from cache map in
	 * {@link InternetMediaForServletBean}
	 */
	@Schedule(second = "0", minute = "*/5", hour = "*", persistent = false)
	private void mediaMapClearSchedule() {
		System.err.println("[Media Map Clear Schedule] : " + new Date());
		mediaBean.clearMap();
		System.err.println("[Media Map Clear Schedule] : " + new Date());

	}

	/**
	 * Periodically get and remove all outdated {@link ActivationToken}
	 */
	@Schedule(second = "0", minute = "*/1", hour = "*", persistent = false)
	private void outdatedActivationTokenSchedule() {
		System.err.println("[Outdated Activation Token Schedule] : " + new Date());
		cleanActivationToken();
		System.err.println("[Outdated Activation Token Schedule] : " + new Date());

	}

	/**
	 * 
	 */
	@Schedule(second = "0", minute = "*/1", hour = "*", persistent = false)
	private void offerMailingSchedule() {
		System.err.println("[Offer Mailing Schedule] : " + new Date());
		newOfferMailing();
		customMailing();
		System.err.println("[Offer Mailing Schedule] : " + new Date());

	}

	private void activeOffers() {

		List<TravelOffer> readyTravel = factoryBean.getFullyOfferByStatus(TravelOffer.TYPE_READY);

		for (TravelOffer travel : readyTravel) {

			try {
				travel.setType(TravelOffer.TYPE_ACTIVE);
				travel.setActivationDate(new Date());

				PlaneTicket planeTicket = travel.getPlanesList().stream()
						.min(Comparator.comparing(PlaneTicket::getTicketPrice)).get();
				HotelReservation hotelReservation = travel.getHotelsList().stream()
						.min(Comparator.comparing(HotelReservation::getPrice)).get();
				travel.setMinPrice(travel.getOfficeCommission() + hotelReservation.getPrice()
						+ planeTicket.getTicketPrice() * hotelReservation.getPersonInRoom());

				fillMailQueue(factoryBean.saveOffer(travel));
			} catch (Exception e) {
				travel.setType(TravelOffer.TYPE_PREPARED_TRANSPORT);
				factoryBean.saveOffer(travel);
			}
		}

	}

	private void cleanActivationToken() {
		List<ActivationToken> tokenList = clientUserBean
				.outdatedActivationTokens(Date.from(Instant.now().minus(Duration.ofDays(1))));
		for (ActivationToken activationToken : tokenList) {
			ClientAccount clientAccount = activationToken.getClientAccount();
			clientUserBean.removeActivationToken(activationToken);
			clientUserBean.removeClientAccount(clientAccount);
		}

	}

	private void newOfferMailing() {
		AppProperty appProperty = mailBean.findAppProperty("LastMailingDate");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date lastOfferActivationDate = null;
		try {
			lastOfferActivationDate = dateFormat.parse(appProperty.getMagicValue());
		} catch (ParseException e) {
			System.err.println("[Date Parse Exception]");
		}

		List<TravelOffer> travelOffers = mailBean.getOfferUntilDate(lastOfferActivationDate);
		if (travelOffers.size() > 0) {
			NewsletterMailContent newsletterMailContent = new NewsletterMailContent(
					"http://localhost:8080/travel-agency-web/", "Media?media", "travelsDetails.xhtml?travel");
			newsletterMailContent.setSubject("Biuro podróży newsletter");
			travelOffers.forEach(t -> newsletterMailContent.appendTravel(t));
			newsletterMailContent.bulid();
			List<String> mailAddressList = mailBean.activeNewsletterMails();
			mailAddressList.forEach(m -> mailBean.sendMail(new NewsletterMail(m, newsletterMailContent)));
			Date newDate = travelOffers.stream().map(TravelOffer::getActivationDate).max(Date::compareTo).get();
			appProperty.setMagicValue(newDate.toString());
			mailBean.saveAppProperty(appProperty);
		}

	}

	private void fillMailQueue(TravelOffer travelOffer) {
		List<Newsletter> newsletters = mailBean.findNewsletterByCountry(travelOffer.getAddress().getCountry());
		for (Newsletter newsletter : newsletters) {
			CustomNewsletter customNewsletter = mailBean.findCustomNewsletter(newsletter.getClientAccount());
			customNewsletter.getTravelOffers().add(travelOffer);
			customNewsletter = mailBean.saveCustomNewsletter(customNewsletter);

		}

	}

	private void customMailing() {
		List<CustomNewsletter> customNewsletters = mailBean.findCustomNewsletter();
		System.err.println("[Custom Newsletters] size : " + customNewsletters.size());
		for (CustomNewsletter customNewsletter : customNewsletters) {
			sendCustomNewsletter(customNewsletter);
			customNewsletter.getTravelOffers().clear();
			mailBean.saveCustomNewsletter(customNewsletter);
		}
	}

	private void sendCustomNewsletter(CustomNewsletter customNewsletter) {

		NewsletterMailContent newsletterMailContent = new NewsletterMailContent(
				"http://localhost:8080/travel-agency-web/", "Media?media", "travelsDetails.xhtml?travel");
		newsletterMailContent.setSubject(
				"Biuro podróży newsletter - " + customNewsletter.getTravelOffers().get(0).getAddress().getCountry());
		customNewsletter.getTravelOffers().forEach(t -> newsletterMailContent.appendTravel(t));
		newsletterMailContent.bulid();
		mailBean.sendMail(new NewsletterMail(customNewsletter.getClientAccount().getMail(), newsletterMailContent));
	}

}
