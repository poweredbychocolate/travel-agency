/**
 * 
 */
package entries.travel;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.mockito.Spy;

import entries.client.Person;

/**
 * @author Dawid
 * @version 1.0
 */
public class ClientTravelCardTest {

	@Spy
	List<Person> list = Arrays.asList(new Person(), new Person(), new Person(), null, null, new Person());

	/**
	 * Test method for
	 * {@link entries.travel.ClientTravelCard#getAccompanyingPersons()}.
	 */
	@Test
	public void testGetAccompanyingPersons() {

		ClientTravelCard travelCard = new ClientTravelCard();
		travelCard.setAccompanyingPersons(list);
		Person person = list.get(5);
		assertNull("Client account exist", travelCard.getClientAccount());
		assertNotNull("Person list not found", travelCard.getAccompanyingPersons());
		assertNotSame("Persons is same", person, travelCard.getAccompanyingPersons().get(2));
		assertNull("Person is not null", travelCard.getAccompanyingPersons().get(4));
		assertSame("Person is not same", person, travelCard.getAccompanyingPersons().get(5));
		person = null;
		list = null;
		travelCard = null;
	}

	/**
	 * Test method for {@link entries.travel.ClientTravelCard#getTravelOffer()}.
	 */

	@Test
	public void testGetTravelOffer() {

		ClientTravelCard travelCard = new ClientTravelCard();
		assertNull("Not null", travelCard.getTravelOffer());
		travelCard.setTravelOffer(new TravelOffer());
		assertNotNull("Null value", travelCard.getTravelOffer());
	}

}
