package enterpriseBean.client;

import static org.junit.Assert.*;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import entries.client.ActivationToken;
import entries.client.ActivationToken_;
import entries.client.ClientAccount;

@RunWith(MockitoJUnitRunner.class)
public class ClientUserBeanTest {

	private HashMap<Integer, ClientAccount> clientAccounts = new HashMap<>();
	private HashMap<Integer, ActivationToken> activationTokens = new HashMap<>();

	@Mock
	private EntityManager entityManager;
	@Mock
	CriteriaQuery<ActivationToken> query;
	@Mock
	CriteriaBuilder builder;
	@Mock
	Root<ActivationToken> tokenRoot;
	@Mock
	TypedQuery<ActivationToken> typedQuery;

	@InjectMocks
	private ClientUserBean clientUserBean;

	@Test
	public void testSaveClientAccount() {
		Mockito.when(entityManager.merge(Mockito.any(ClientAccount.class))).thenAnswer(m -> {
			ClientAccount account = m.getArgument(0);
			if (clientAccounts.containsKey(account.getId())) {
				return true;
			} else {
				clientAccounts.put(account.getId(), account);
				return account;
			}
		});

		ClientAccount clientAccount = new ClientAccount();
		clientAccount.setId(2);
		assertNotNull(clientUserBean.saveClientAccount(clientAccount));
		clientAccount.setId(3);
		assertNotNull(clientUserBean.saveClientAccount(clientAccount));
		assertNull(clientUserBean.saveClientAccount(clientAccount));
		assertNull(clientUserBean.saveClientAccount(clientAccount));
		clientAccounts.clear();
	}

	@Ignore
	public void testFindUser() {
		fail("Not yet implemented"); // TODO
	}

	@Ignore
	@Test
	public void testOutdatedActivationTokens() {
		Mockito.when(entityManager.getCriteriaBuilder()).thenReturn(builder);
		Mockito.when(builder.createQuery(ActivationToken.class)).thenReturn(query);
		Mockito.when(query.from(ActivationToken.class)).thenReturn(tokenRoot);
		Mockito.when(entityManager.createQuery(query)).thenAnswer(m -> {
			query.where(builder.lessThanOrEqualTo(tokenRoot.get(ActivationToken_.generatedDate),
					Date.from(Instant.now().minus(Duration.ofDays(1)))));
			query.select(tokenRoot);
			return typedQuery;
		});

		activationTokens.clear();
		for (int i = 1; i < 11; i++) {
			ActivationToken activationToken = new ActivationToken();
			activationToken.setGeneratedDate(Date.from(Instant.now().minus(Duration.ofDays(1))));
			activationToken.setId(i);
			activationTokens.put(i, activationToken);

		}
		List<ActivationToken> outdated = clientUserBean
				.outdatedActivationTokens(Date.from(Instant.now().minus(Duration.ofDays(1))));
		assertEquals(10, outdated.size());
	}

}
