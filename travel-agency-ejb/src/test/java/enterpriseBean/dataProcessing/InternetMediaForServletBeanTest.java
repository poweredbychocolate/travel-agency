/**
 * 
 */
package enterpriseBean.dataProcessing;

import static org.junit.Assert.*;

import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import entries.InternetMedia;

/**
 * @since 25.06.2018
 * @author Dawid
 * @version 1.0
 *
 */

@RunWith(MockitoJUnitRunner.class)
public class InternetMediaForServletBeanTest {
	@Mock
	private EntityManager entityManager;
	@Spy
	private ConcurrentHashMap<Integer, InternetMedia> internetMediaCache = new ConcurrentHashMap<Integer, InternetMedia>();
	@InjectMocks
	private InternetMediaForServletBean internetMediaBean;

	@Before
	public void before() {
		internetMediaCache.put(1, new InternetMedia(1, new byte[40], "TEST 01"));
		internetMediaCache.put(2, new InternetMedia(2, new byte[20], "TEST 02"));
		internetMediaCache.put(3, new InternetMedia(3, new byte[10], "TEST 03"));
		internetMediaCache.put(4, new InternetMedia(4, new byte[30], "TEST 04"));
	}

	@After
	public void after() {
		internetMediaCache.clear();
	}

	/**
	 * Test method for
	 * {@link enterpriseBean.dataProcessing.InternetMediaForServletBean#find(java.lang.Integer)}.
	 */

	@Test
	public void testFind() {

		assertNotNull("Media Not Found", internetMediaBean.find(1));
		// TODO add test for index outside the map
		// assertNull("Value is not null", internetMediaBean.find(10));
	}

	/**
	 * Test method for
	 * {@link enterpriseBean.dataProcessing.InternetMediaForServletBean#clearMap()}.
	 */

	@Test
	public void testClearMap() {

		assertNotEquals(0, internetMediaCache.size());
		internetMediaBean.clearMap();
		assertEquals(0, internetMediaCache.size());
	}

}
